#include "../Network/Client.h"
#include "Group.h"
#include "Base.h"
#include <cmath>


Group::Group() : mSpeed(70), mSprite(0), mRing(0), mRemove(false)
{}

Group::Group(Base * dest, Base * origin, float nrUnits, int id, int teamId, Manager * mgr) : mSpeed(70), mSprite(0), mRemove(false)
{
	init(dest, origin, nrUnits, id, teamId, mgr);
}

Group::~Group()
{}

bool Group::update(float dt)
{
	if (mNrOfUnits < 0)
		return false;
	mPosition += mDir * mSpeed * dt;
	mDistMoved += mSpeed * dt;
	if(mDistMoved > mToStop)
	{
		return false;
	}
	return true;
}

std::string Group::msgGroup()
{
	std::stringstream ss;
	ss << MSG_GROUP << " ";
	ss << mId << " ";
	ss << mColor.GetHWColor()<< " ";
	ss << mNrOfUnits << " ";
	ss << mDistMoved << " ";

	std::string temp;
	getline(ss, temp, '\n');

	return temp;
}


std::string Group::msgMove()
{
	std::stringstream ss;
	ss << MSG_MOVE << " ";
	ss << mTeamID << " ";
	ss << mId << " ";
	ss << mColor.GetHWColor() << " ";
	ss << mToStop << " ";
	ss << mNrOfUnits << " ";
	ss << mOrigin->getId() << " ";
	ss << mDestination->getId() << " ";
	
	
	std::string temp;
	getline(ss, temp, '\n');
	return temp;
}

std::string Group::msgArrive()
{
	std::stringstream ss;
	ss << MSG_ARRIVE << " ";
	ss << mTeamID << " ";
	ss << mId << " ";
	ss << (int)mNrOfUnits << " ";
	ss << mDestination->getId();
	ss << mColor.GetHWColor() << " ";
	
	std::string temp;
	getline(ss, temp, '\n');
	return temp;
}

void Group::init(Base * dest, Base * origin, float nrUnits, int id, int teamId, Manager * mgr)
{
	mId = id;
	mTeamID = teamId;

	mMgr = mgr;
	mDistMoved = 0;
	mTeamID = origin->getTeamId();
	mOrigin = origin;
	mDestination = dest;
	mPosition = origin->getPos();
	mColor = origin->getColour();
	mNrOfUnits = nrUnits;

	mDir = dest->getPos() - mPosition;
	mToStop = mDir.Length();
	mDir *= 1 / mToStop;
	mSize = 20 + log(mNrOfUnits) * 3;
	if (mNrOfUnits <= 0)
		mSize = 20;

	float temp = mSize * 0.5;

	//hgeColor tCol = hgeColor(1, 1, 1, 1) - mColor;
	hgeColorHSV tCol2(mColor.GetHWColor());
	hgeColorHSV tCol = hgeColorHSV(1, 1, 1, 1) - tCol2;
	tCol.a = 1;

	mTextCol = hgeColor(tCol.GetHWColor());

	mFont = mgr->getFontMgr()->getFont("Font/base.fnt", "group");
	mFont->SetColor(tCol.GetHWColor());

	if (mSprite == 0)
	{
		mSprite = new hgeSprite(mMgr->getTexMgr()->getTexture("Image/base.png"), -temp, - temp, mSize, mSize);
		mSprite->SetTextureRect(0, 0, 128, 128, false);
		mSprite->SetHotSpot(temp, temp);
		mSprite->SetColor(mColor.GetHWColor());
	}
	

	mRing = new hgeSprite(mMgr->getTexMgr()->getTexture("Image/base.png"), -temp - 2.5, - temp - 2.5, mSize + 5, mSize + 5);
	mRing->SetTextureRect(0, 0, 128, 128, false);
	mRing->SetHotSpot(temp + 2.5, temp + 2.5);
	mRing->SetColor(mMgr->getTeamColour(mTeamID).GetHWColor());
}

void Group::render()
{
	if (mNrOfUnits <= 0)
		return;

	mRing->Render(mPosition.x, mPosition.y);
	mSprite->Render(mPosition.x, mPosition.y);
	mFont->printf(mPosition.x, mPosition.y - 7.5, HGETEXT_CENTER, "%i", (int)mNrOfUnits);
}

int Group::getId()
{
	return mId;
}
int Group::getTeamId()
{
	return mTeamID;
}

void Group::setDistMoved(float dist)
{
	mDistMoved = dist;
}

void Group::setId(int id)
{
	mId = id;
}

Base * Group::getDest()
{
	return mDestination;
}

void Group::restore(Base * dest, int units, int id, int teamId, hgeColor col, Manager * mgr)
{
	mDestination = dest;
	mNrOfUnits = units;
	mTeamID = teamId;
	mColor = col;
	mMgr = mgr;
}