#ifndef BASE_H
#define BASE_H


#include "../Manager/Manager.h"
#include <hge.h>
#include <hgecolor.h>
#include <hgevector.h>
#include <float.h>

#include "Group.h"

#include <vector>
#include <cstring>

const int CIRCLE_CORNERS = 20;

enum BaseDisplay
{
	B_FIELD,
	B_TEAM
};

class Base
{

public:
	Base();
	Base(hgeColor col, float x, float y, float inRadie, float size, int id, int teamId, Manager * mgr);
	virtual ~Base();

	void init(hgeColor col, float x, float y, float inRadie, float size, int id, int teamId, Manager * mgr);
	bool update(float dt);
	void renderField(BaseDisplay mode = B_FIELD);
	void renderBase();

	//network communication stuff
	std::string msgBase();
	std::string msgNew(int origin);
	std::string msgCaptured();
	std::string msgSetDestCol();


	void fight(float dt);
	void capture(int id, int size);
	void sacrifice(int units);
	Group * sendGroupTo(Base * dest, int units, int id, int teamId, bool doNothing = false);

	bool buildBase(hgeColor col, float x, float y, int units, int id, Group ** gOut, Base ** bOut, bool doNothing = false);

	void forceBuilt();
	bool isCaptured();

	int getSize();

	float influence(Base &b);
	float distance(Base &b);
	float distance(hgeVector p);
	void addGroup(Group * group);
	void removeGroup(Group * group);

	void setDstColour(hgeColor dst);
	void setColour(hgeColor col);
	void setOutColour(hgeColor col);
	void setPosition(float x, float y);
	void setRadius(float r);
	void setSize(float size);
	void setUnitTime(float time);
	void setUnits(float units)
	{
		mNrOfUnits = units;
	}

	void select();
	void unselect();

	float getRadius();
	hgeColor getColour();
	hgeColor getOutColour();
	hgeVector getDivider(Base &b, float dist = -1);
	float getAlphaAt(hgeVector p);

	void setMode(BaseDisplay mode);
	void resetColour();

	void updateTri();
	void updateQuad();

	void setQuad();
	void setTri();

	void setManger(Manager * mgr);

	bool checkSelection(hgeVector p1, hgeVector p2);

	bool inBase(float x, float y);

	

	float enemySize()
	{
		float s = 0;
		for(size_t i = 0; i < mAttackers.size(); i++)
		{
			if (_isnan(mAttackers[i]->getNrOfUnits()))
				int h = 0;
			else
				s += mAttackers[i]->getNrOfUnits();
		}

		return s;
	}

	std::vector<int> getAttackersId();

	int getId()
	{
		return mID;
	}
	int getTeamId()
	{
		return mTeamID;
	}

	hgeVector getPos()
	{
		hgeVector pos;

		pos.x = mX;
		pos.y = mY;

		return pos;
	}
 
	int getNrOfUnits()
	{
		return (int)mNrOfUnits;
	}

	bool inRadius(float x, float y)
	{
		hgeVector v1;
		v1.x = abs(mX-x);
		v1.y = abs(mY-y);
		float size = sqrt(v1.x*v1.x+v1.y*v1.y);
		if(size < mInRadie)
			return true;
		return false;
	}
	bool inOuterRadius(float x, float y)
	{
		hgeVector v1;
		v1.x = abs(mX-x);
		v1.y = abs(mY-y);
		float size = sqrt(v1.x*v1.x+v1.y*v1.y);
		if(size < mRad)
			return true;
		return false;		
	}

private:
	std::vector<Group *> mAttackers; // Rename to mAttacker for clarification

	float mX, mY;

	hgeColor mOutCol;
	hgeColor mCol;
	hgeColor mDstCol;
	BaseDisplay mMode;

	float mNrOfUnits;

	float mInRadie;
	float mRad;

	int mID;
	int mTeamID;
	int mNewTeam;

	bool mBuilt;
	
	float mAdvancement; // Changing Colour to destination Colour.
	float mSpeed; // Speed for how fast to advance.
	float mSize; // Max population / amount of colour
	float mMaxRatio; // If nrOfUnits more than max, decrease until it reaches max.

	// How much time it takes before next unit spawns.
	float mUnitTime;

private:
	void updateTriVert();

	hgeTriple mTri[CIRCLE_CORNERS];
	hgeSprite * mBase;
	hgeSprite * mRing;
	hgeSprite * mOut;
	hgeSprite * mDest;
	hgeFont * mFont;

	bool mBaseChange;
	bool mTriChange;
	bool mSelected;
	
	int mTBlend;
	
	Manager * mMgr;
};

#endif
