#ifndef PLAYER_H
#define PLAYER_H

#include "Base.h"

class Player
{

public:

	Player();
	virtual ~Player();

	void addBuilding(Base * base);

	//void addBuilding(hgeColor col, float x, float y, float inRadie, float size, int id, HGE * hge);

	void removeBuilding(int at);

	void update(float dt);

	Base * getBase(int at)
	{
		return mBuildings[at];
	}

	int nrOfBuildings()
	{
		return mBuildings.size();
	}


private:

	std::vector< Base * > mBuildings;
	//std::vector<Base> mBuildings;
	int mID;


};

#endif