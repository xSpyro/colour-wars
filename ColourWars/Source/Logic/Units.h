#ifndef UNITS_H
#define UNITS_H

#include <hgecolor.h>
#include <hgevector.h>

class Units
{

public:

	Units();
	virtual ~Units();

	void update(float dt);

	void moveTo(float x, float z);

private:

	
	hgeColor mColor;
	hgeVector mPosition;


	// Size of the visual texture.
	float mSize; 

};

#endif

