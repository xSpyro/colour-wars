#ifndef GAME_H
#define GAME_H

#include <hgefont.h>
#include "../Program.h"
#include "../Scene.h"
#include "Player.h"
#include "../gui/BottomSlider.h"
#include "../GUI/ColorControl.h"
#include "../gui/ImageButton.h"

class Server;
class Client;
class ClientManager;
class Game : public Scene
{

public:
	Game();
	virtual ~Game();

	virtual bool update(float dt);
	virtual void render();
	virtual bool restore();

	bool selectBase(float x, float z, int id);

	virtual void init(Manager * mgr);
	virtual void enterScene();
	virtual void exitScene();
	//virtual void terminate();
	virtual void onPause();
	virtual void onResume();

	void start();

	//for network communication
	void parse(const std::string &msg);
	void connectTo(int port, const char *host);
	void host(int port);
	bool inScreen(float x, float y, float rad);

	//for the slider at the bottom, and to se if someone has won
	void amount();
	void won();

	Base * getBuilder(hgeVector pos);

	

private:
	Base * findBase(int id);
	Group * findGroup(int id);

	void checkInput();
	//for network communication
	void errorMessage(const std::string &msg);

	// Return NULL if no bases were selected.
	Base * selectBase(int id);
	Base * selectBase();

	// if false, invalid place for building.
	bool buildBase(hgeColor col, float x, float y, int units, int id, int teamId, bool forceBuilt = false);
	void updateUnitMovement(float dt);

	// Init the main bases of 2 players.
	void initBases();

	// Deletes everything, and resets.
	void resetGame();

	void playSound(std::string desc);

	void renderSRect();
		
private:

	void parseBase(std::stringstream &stream);
	void parseGroup(std::stringstream &stream);
	void parseSendMove(std::stringstream &stream);
	void parseSendNewBase(std::stringstream &stream);
	void parseBaseCapture(std::stringstream &stream);
	void parseSacrifice(std::stringstream &stream);
	void parseRmvGroup(std::stringstream &stream);
	void parseArrive(std::stringstream &stream);
	void parseDestCol(std::stringstream &stream);

	void multiSelect(std::vector<Base *> b, hgeVector p1, hgeVector p2);
	void checkSelection();

	Group * removeGroup(int id);


private:

	friend class Server;
	
	bool mHost;
	bool mConnected;
	int mPlayerID;
	int mEnemyID;

	std::string mLine;

	Player mPlayers[2];
	

	/**
	 * This is main vector with ALL the bases of game.
	 * Each player will have own vector with pointer to bases for their own use.
	 */
	
	std::vector<Base *> mBases;

	std::vector<Group *> mGroups;
	std::vector<Group *> mFighting;
	std::vector<Group *> mToRemove;

	HTARGET mTarget;
	hgeSprite * mField;

	hgeFont * mFont;

	Base * mSelected;
	BaseDisplay mMode;

	bool mRunning;
	bool mArmySelected;
	bool mEnemySelected;
	HTARGET mTar; // Print all fields to this target so that we can set the bases actuall colour and not just their output
	hgeSprite * mFields; // Gets texture from mTar

	int mWidth;
	int mHeight;

	std::vector<float> mUnitCapacity;
	std::vector<float> mBaseLimit;
	std::vector<int> mNrOfBases;

	Slider mSlider; // Chosing how many units should leave the base.

	bool mSacrifice;
	float mSliderTemp; // Used to store what value on the slider there were before sacrifice was selected
	
	/**
	 * Temp variables, delete if not needed.
	 */
	int mSent;

	hgeGUI *tGUI;


	ColorControl mControl;
	hgeColor mCol;

	// F�r h�ger sida, kommer vara under sliders.
	hgeSprite * mBackground;
	
	bool mInit;

	//for the slider at the bottom

	float	mArea1,
		mArea2;
	BottomSlider	*mBottomSlider;

	ClientManager *mClient;

	Server * mServer;
	static const int mPort = 8888;

	//new
	int mID,
		mLastID;

	//det som kan beh�va sparas f�r rolig info

	std::vector <Base*> mSelection;

	float mMousePosX, mMousePosY, mOldMousePosX, mOldMousePosY;
	hgeVector mOldMousePos, mMousePos;


};

#endif