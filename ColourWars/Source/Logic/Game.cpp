#include "../Network/Server.h"
#include "../Network/Client.h"
#include "../Network/ClientManager.h"

#include "Game.h"

Game::Game() : mUnitCapacity(), mNrOfBases(), mBaseLimit()
{
	mServer = new Server();
	mClient = new ClientManager();
	mClient->setListener(this);
	mHost = false;
	mConnected = false;
	mSacrifice = false;

	mRunning = true;
	mArmySelected = false;
	mEnemySelected = false;
	mSelected = NULL;
	mMode = B_FIELD;
	mInit = false;

	mSent = 0;

	//mPlayers[0].addBuilding(hgeColor(20, 35, 50, 1), 40, 60, 30, 100, 1, mMgr);
	//mBases.push_back(Base(hgeColor(20, 35, 50, 1), 40, 60, 30, 100, 1, mMgr));



	//mClient->connectTo(mPort, "194.47.150.197");
	//host(mPort);
	mServer->setGame(this);


	mWidth  = 1024;
	mHeight = 768;

	/** 
	 * If player has hosted, id will be 0, else use 1, as id.
	 * NOTE: THIS ID IS NOT FOR VECTOR INDEX.
	 */
	mPlayerID = 1;
	mEnemyID  = 0;


}

Game::~Game()
{

}

void Game::parse(const std::string &msg)
{
	mLine = msg;
	
	if (msg.find("ERROR") != std::string::npos ) 
		errorMessage(msg);


	std::stringstream stream(msg);
	int temp = -1;
	stream >> temp;

	switch(temp)
	{
	case MSG_CONNECT: 
		break;
	case MSG_BASE:
		parseBase(stream);
		break;
	case MSG_NEWBASE:
		parseSendNewBase(stream);
		break;
	case MSG_MOVE:
		parseSendMove(stream);
		break;
	case MSG_GROUP:
		parseGroup(stream);
		break;
	case MSG_BASECAPTURE:
		parseBaseCapture(stream);
		break;
	case MSG_RMVGROUP:
		parseRmvGroup(stream);
		break;
	case MSG_ARRIVE:
		parseArrive(stream);
		break;
	case MSG_SETDESTCOL:
		parseDestCol(stream);
		break;
	default:
		std::cout << "Illegal message type: " << temp << ". Whole message: " << msg << "\n";
		break;
	}	
}

void Game::errorMessage(const std::string &msg)
{
	std::cout<<msg<<std::endl;
}

void Game::host(int port)
{
	mServer->run(port);
	mHost = true;
	mPlayerID = 0;
	mEnemyID = 1;
		
	if (mClient->connectTo(port, "localhost") == 1)
		mConnected = true;
	
}

void Game::connectTo(int port, const char *host)
{
	if ( mClient->connectTo(port, host) ==  1)
		mConnected = true;
}

bool Game::update(float dt)
{

	if ( !mLine.empty() )
		std::cout << mLine << '\n';
	mLine = "";

	if ( mHost ) //just for the one who is hosting
	{				
		if ( !mServer->allConnected())
			mServer->acceptClients();
		else if (!mInit)
		{
			initBases();			
			mClient->sendStart();
			mInit = true;
		}

		mServer->update(dt);
	}


	//shall happen always, for everyone
	mClient->recieve();


	HGE * hge = mMgr->getHge();
	if (hge->Input_GetKeyState('A'))
		mMode = B_TEAM;
	else
		mMode = B_FIELD;


	while (mUnitCapacity.size() < 2)
	{
		mUnitCapacity.push_back(float(0));
	}
	while (mBaseLimit.size() < 2)
	{
		mBaseLimit.push_back(int(0));
	}
	while (mNrOfBases.size() < 2)
	{
		mNrOfBases.push_back(int(0));
	}

	mUnitCapacity[0] = 0;
	mUnitCapacity[1] = 0;
	mNrOfBases[0] = 0;
	mNrOfBases[1] = 0;

	for(size_t i = 0; i < mBases.size(); i++)
	{
		mBases[i]->resetColour();
	}
	for(size_t i = 0; i < mBases.size(); i++)
	{
		for(size_t j = i+1; j < mBases.size(); j++)
		{
			mBases[i]->influence(*mBases[j]);
		}
	}
	for(size_t i = 0; i < mBases.size(); i++)
	{
		mUnitCapacity[mBases[i]->getTeamId()] += mBases[i]->getSize();
		mNrOfBases[mBases[i]->getTeamId()]++;

		mBases[i]->update(dt);
		if (mBases[i]->isCaptured())
			mClient->sendBaseCapture(mBases[i]);
		mBases[i]->updateQuad();
	}

	for (int n = 0; n < mBaseLimit.size(); ++n)
	{
		float t = (pow((float)mUnitCapacity[n], (float)(0.34)) * 1.5);
		mBaseLimit[n] = t;
	}

	updateUnitMovement(dt);

	//tColor = mControl.getColor();

	mCol = mControl.getColor();

	mID = tGUI->Update(dt);
	checkInput();

	amount();
	mBottomSlider->update(dt, mArea1, mArea2);

	won();

	switch(mID)
	{
	case 1:
		mServer->end();
		mMgr->getSceneMgr()->changeScene("LoseScene",1);
		break;

	case 2:
		for ( int i = 0; i<mSelection.size(); ++i ) 
			mClient->sendSetDestCol(mSelection.at(i)->getTeamId(),mSelection.at(i)->getId(), mControl.getColor());
		break;

	case 3:
		for ( int i = 0; i<mSelection.size(); ++i )
		{
			int toDie = mSlider.getValue() * mSelection.at(i)->getNrOfUnits();
			mClient->sendSacrifice(mSelection.at(i)->getId(), mPlayerID, toDie);
		}
		break;

	}

	return true;
}
void Game::render()
{
	hgeColor tColor = mControl.getColor();
	HGE * hge = mMgr->getHge();
	hge->Gfx_BeginScene(mTarget);
	hge->Gfx_SetTransform(0, 0, 0, 0, 0, (float) 128 / 1024, (float) 128 / 768);
	hge->Gfx_Clear(0);

	for(size_t i = 0; i < mBases.size(); i++)
		mBases[i]->renderField(mMode);

	hge->Gfx_EndScene();

	hge->Gfx_BeginScene();
	hge->Gfx_Clear(0);

	mField->RenderStretch(0, 0, 1024, 768);

	for(size_t i = 0; i < mBases.size(); i++)
	{
		mBases[i]->renderBase();
	}

	if (mOldMousePos != hgeVector(-1, -1))
		renderSRect();

	hgeColor mPlaceCol = hgeColor(0.6f, 0.3f, 0.5f, 1.0f);
	mFont->printf(5, 5, HGETEXT_LEFT, "FPS: %d", hge->Timer_GetFPS());
	// TA BORT EFTER TESTING
	//mFont->printf(5, 30, HGETEXT_LEFT, "Sent: %d", mSent);
	//if(mSelected == NULL)
	//	mFont->printf(5, 55, HGETEXT_LEFT, "COLOR: %f %f %f", tColor.r, tColor.g, tColor.b);
	//else
	//	mFont->printf(5, 55, HGETEXT_LEFT, "Selected: %i, %i", mSelected->getId(), mSelected->getTeamId());

	

	for(size_t i = 0; i < mGroups.size(); i++)
	{
		mGroups[i]->render();
	}
	
	/*
	// Rita ut tempor�rt enheter som r�r sig, TA BORT EFTER TESTING
	for(size_t i = 0; i < mGroups.size(); i++)
		spriten[0]->Render(mGroups[i]->getPos().x, mGroups[i]->getPos().y);
	*/
	mBackground->Render(mWidth-150, 0);
	mSlider.render();
	mControl.render();
	tGUI->Render();
	
	float u = 0;
	if(!mSelection.empty())
	{
		for ( int i = 0; i<mSelection.size(); ++i ) 
			u += mSelection.at(i)->getNrOfUnits();
	}

	mFont->printf(mWidth-5, 290, HGETEXT_RIGHT, "%d", (int)(mSlider.getValue() * (float)u) );
	mFont->printf(mWidth-125, 0, HGETEXT_LEFT, "1   2    3");
	mFont->printf(mWidth-140, 240, HGETEXT_LEFT, "Unit Capacity\n%i", (int)mUnitCapacity[mPlayerID]);
	mFont->printf(mWidth-140, 160, HGETEXT_LEFT, "Base Limit\n%.1f", (mBaseLimit[mPlayerID] - 0.05)); // To avoid rounding
	mFont->printf(mWidth-140, 200, HGETEXT_LEFT, "Nr Of Bases\n%i", mNrOfBases[mPlayerID]);

	mBottomSlider->render();
	

	if (!mInit && mHost)
	{
		mFont->Render(mWidth/2, mHeight/2, HGETEXT_CENTER, "Waiting for connections...");
	}

	hge->Gfx_EndScene();
}
bool Game::selectBase(float x, float z, int id)
{
	hgeVector v;
	mMgr->getHge()->Input_GetMousePos(&v.x, &v.y); 

	
	
		


	/**
	 * If friendly base is selected, and has units, send 
	 * selected grou
	 */
	
	
	
	// If selection is possible.
	return true;
}

bool Game::restore()
{
	if (mTar && mFields)
	{
		mField->SetTexture(mMgr->getHge()->Target_GetTexture(mTarget));
	}
	return true;
}

void Game::init(Manager * mgr)
{
	mUnitCapacity.push_back(0);
	mUnitCapacity.push_back(0);
	mBaseLimit.push_back(0);
	mBaseLimit.push_back(0);
	mNrOfBases.push_back(0);
	mNrOfBases.push_back(0);

	Scene::init(mgr);
	HGE * hge = mMgr->getHge();
	mTarget = hge->Target_Create(128, 128, false);
	mField = new hgeSprite(hge->Target_GetTexture(mTarget), 0, 0,hge->System_GetState(HGE_SCREENWIDTH), hge->System_GetState(HGE_SCREENHEIGHT));
	mField->SetTextureRect(0, 0, 128, 128);

	mFont = mgr->getFontMgr()->getFont("Font/Text15.fnt");
	mFont->SetColor(0xffffffff);

	mMgr->setTeamColour(0, hgeColor(0, 0, 1, 1));
	mMgr->setTeamColour(1, hgeColor(1, 0, 0, 1));
	mMgr->setTeamColour(2, hgeColor(0, 1, 0, 1));


	/**
	 * Color Control grejer!!!!!!!!!!!!!!!!
	 */

	HTEXTURE backCCT = mMgr->getTexMgr()->getTexture("Image/backC.png");
	hgeSprite * backCC = new hgeSprite(backCCT, 0, 0, 150, 150);

	HTEXTURE red   = mMgr->getTexMgr()->getTexture("Image/R.png");
	HTEXTURE green = mMgr->getTexMgr()->getTexture("Image/G.png");
	HTEXTURE blue  = mMgr->getTexMgr()->getTexture("Image/B.png");

	HTEXTURE tex2 = mMgr->getTexMgr()->getTexture("Image/slideBack.png");
	hgeSprite * background = new hgeSprite(tex2, 0, 0, 30, 140);

	tGUI = new hgeGUI();

	HTEXTURE cColor = mMgr->getTexMgr()->getTexture("Image/Base.png");
	hgeSprite * cSprite = new hgeSprite(cColor, 0, 0, 20, 20);
	cSprite->SetTextureRect(0, 0, 128, 128, false);
		
	mControl.init(tGUI, mWidth-150, 0, 150, 150, backCC, background, red, green, blue);
	mControl.setColor(cSprite);

	HTEXTURE slider = mMgr->getTexMgr()->getTexture("Image/slider.png");
	mSlider.init(tGUI, 0, 1, 80, mWidth-30, 160, 30, 120, slider, 0, 0, 30, 20, background, true); 

	HTEXTURE back = mMgr->getTexMgr()->getTexture("Image/rightBack2.png");
	mBackground = new hgeSprite(back, 0, 0, mWidth, mHeight); //mWidth, 0, mWidth - 650, mHeight);
	mBackground->SetTextureRect(0, 40, mWidth, mHeight, false);

	mBottomSlider = new BottomSlider(0, mHeight - 20, mWidth - 150, mHeight, tGUI, mWidth, mHeight, mMgr);

	HTEXTURE temp[2];
	temp[0] = mMgr->getHge()->Texture_Load("Image/Buttons/mainmenuFULL.png");
	temp[1] = mMgr->getHge()->Texture_Load("Image/Buttons/mainmenu2FULL.png");
	//mMenuButton = new ImageButton(15, mWidth-150, mHeight-32, 150, 32, temp);
	tGUI->AddCtrl(new ImageButton(1, mWidth-150, mHeight - 40, 150, 40, temp));

	temp[0] = mMgr->getHge()->Texture_Load("Image/Buttons/colorchangeFULL.png");
	temp[1] = mMgr->getHge()->Texture_Load("Image/Buttons/colorchange2FULL.png");
	tGUI->AddCtrl(new ImageButton(2, mWidth - 150, mHeight/ 3 + 60, 150, 40, temp));

	temp[0] = mMgr->getHge()->Texture_Load("Image/Buttons/sacrificeFULL.png");
	temp[1] = mMgr->getHge()->Texture_Load("Image/Buttons/sacrifice2FULL.png");
	tGUI->AddCtrl(new ImageButton(3, mWidth - 150, mHeight/ 3 + 100, 150, 40, temp));


}

void Game::enterScene()
{
	tGUI->Enter();
	mSlider.setValue(0.5);

	mCol.r = 1;
	mCol.g = 1;
	mCol.b = 1;

	mCol = (1.0/3.0) * mCol * 255;

	mControl.setValue(mCol.r, mCol.g, mCol.b);
	mOldMousePos.x = -1;
	mOldMousePos.y = -1;
}

void Game::exitScene()
{
	tGUI->Leave();
	for (int n = 0; n < mBases.size(); ++n)
		delete mBases[n];
	mBases.clear();

	for (int n = 0; n < mGroups.size(); ++n)
		delete mGroups[n];
	mGroups.clear();
}

void Game::onPause()
{
	tGUI->Enter();
}

void Game::onResume()
{
	tGUI->Leave();
}

void Game::start()
{}

void Game::checkInput()
{
	HGE * hge = mMgr->getHge();
	int sent = 0;

	hge->Input_GetMousePos(&mMousePosX, &mMousePosY);
	mMousePos.x = mMousePosX;
	mMousePos.y = mMousePosY;

	//send destination colour
	if (hge->Input_KeyDown('D'))
	{
		for ( int i = 0; i<mSelection.size(); ++i ) 
			mClient->sendSetDestCol(mSelection.at(i)->getTeamId(),mSelection.at(i)->getId(), mControl.getColor());		
	}

	// Placing a base
	if (hge->Input_KeyDown(HGEK_LBUTTON))
	{
		//for multiselect
		hge->Input_GetMousePos(&mOldMousePosX, &mOldMousePosY);
		
		mOldMousePos.x = mOldMousePosX;
		mOldMousePos.y = mOldMousePosY;

		// If not selected, try to select a friendly base.

		if(mMousePos.x <= mWidth-150)
		{
			for (int n = 0; n < mSelection.size(); ++n )
			{
				mSelection[n]->unselect();
			}
			mSelection.clear();		

			Base * b = selectBase();

			if (b != 0)
			{
				mSelection.push_back(b);
				if(!mSelection.empty())
				{
					mSelection.back()->select();
					hgeColor tColor = mControl.getColor();
					std::cout << "Color: " << tColor.r << " " << tColor.g << " " << tColor.b << "\n";
					//mControl.setValue(col.r, col.g, col.b);
				}
			}
		}		
	}// if left mouse button was pressed
	if (hge->Input_KeyUp(HGEK_LBUTTON) && mOldMousePos != hgeVector(-1, -1))
	{
		multiSelect(mBases, mOldMousePos, mMousePos);
		mOldMousePos = hgeVector(-1, -1);
	}

	if(hge->Input_GetKeyState('S') != mSacrifice && !mSelection.empty())
	{
		if (mSacrifice)
		{
			for ( int i = 0; i<mSelection.size(); ++i )
			{
				int toDie = mSlider.getValue() * mSelection.at(i)->getNrOfUnits();
				mClient->sendSacrifice(mSelection.at(i)->getId(), mPlayerID, toDie);				
			}
			mSlider.setValue(mSliderTemp);
		}
		else
		{
			mSliderTemp = mSlider.getValue();
			mSlider.setValue(0);
		}
		mSacrifice = !mSacrifice;
	}

	if (hge->Input_GetKey() == HGEK_RBUTTON)
	{
		if(!mSelection.empty())
		{
			//mSlider.setMode(0, mSelected->getNrOfUnits(), HGESLIDER_SLIDER);
			Base * temp = selectBase();
			if (temp != 0 )
			{
				for ( int i = 0; i<mSelection.size(); ++i ) 
				{
					mSent = mSelection.at(i)->getNrOfUnits();

					float FLOAT  = mSlider.getValue();
					mSent = FLOAT * mSent;

					if(mSent >= 1)
					{
						Group * group = mSelection.at(i)->sendGroupTo(temp, mSent, -1, mPlayerID, true);

						mClient->sendMove(group);

						delete group;
					}
				}
		
			}
			
			else if( mSelection.back()->getTeamId() == mPlayerID)
			{
				hgeVector p;
				hge->Input_GetMousePos(&p.x, &p.y);

				std::vector<Base *> bases;
				for(size_t i = 0; i < mBases.size(); i++)
				{
					if( mBases[i]->inRadius(p.x, p.y) )
						return;
					else if( !mBases[i]->inRadius(p.x, p.y) && mBases[i]->inOuterRadius(p.x, p.y) )
					{
						// mBases[i]->getRadius()) )
						bases.push_back(mBases[i]);
					}
				}

				for(size_t i = 0; i < bases.size(); i++)
				{
					if( bases[i]->inOuterRadius(p.x, p.y) && bases[i]->getTeamId() == mPlayerID)
					{
						Base * close = getBuilder(mMousePos);
						mClient->sendNewBase(p.x, p.y, -1, mPlayerID, mControl.getColor(), false, close->getId());
						
						break;
					}
				}
				

				//buildBase(hgeColor(0.0f, 0.0f, 0.0f, 1.0f), p.x, p.y, 10);
			}
		}
		
	}

	int mw = hge->Input_GetMouseWheel();
	if (mw != 0)
	{
		/**
		 * Om S �r nere, ta value fr�n slider, och ange det som sacrafice units.
		 * 123 �r f�r RGB �ndra value.                 
		 */
		float val = 0; 
		float speed = 12.75;

		
		if (hge->Input_GetKeyState('1'))
		{
			val = mControl.getRValue() + speed * mw;
			mControl.setRValue(val);
		}
		else if (hge->Input_GetKeyState('2'))
		{
			val = mControl.getGValue() + speed * mw;
			mControl.setGValue(val);
		}
		else if (hge->Input_GetKeyState('3'))
		{
			val = mControl.getBValue() + speed * mw;
			mControl.setBValue(val);
		}
		else
		{
			val = mSlider.getValue() + 0.05 * mw;
			mSlider.setValue(val);
		}
	}

	hgeColor before = mCol;
	hgeColor after = mControl.getColor();
	int changeOn = -1;
	float rest;
	float c1, c2;

		// Om color slider har �ndrats, regulera v�rdet.
	if( after.r != before.r )
	{
		changeOn = 0;
		rest = before.r - after.r;
		c1 = after.g;
		c2 = after.b;
	}
	else if( after.g != before.g ) 
	{
		changeOn = 1;
		rest = before.g - after.g;
		c1 = after.b;
		c2 = after.r;
	}
	else if( after.b != before.b ) 
	{
		changeOn = 2;
		rest = before.b - after.b;
		c1 = after.r;
		c2 = after.g;
	}

	if (changeOn != -1)
	{
		float p1, p2;
		p1 = - rest * 0.5;
		p2 = - rest * 0.5;

		if (c1 < p1)
		{
			p2 += p2 - c1;
			p1 = c1;
		}
		if (c2 < p2)
		{
			p1 += p1 - c2;
			p2 = c2;
		}

		int i1 = (c1 - p1) * 255, 
			i2 = (c2 - p2) * 255;

		switch(changeOn)
		{
		case 0:
			mControl.setGValue(i1);
			mControl.setBValue(i2);
			break;
		case 1:
			mControl.setBValue(i1);
			mControl.setRValue(i2);
			break;
		case 2:
			mControl.setRValue(i1);
			mControl.setGValue(i2);
			break;
		}
	}
}

Base * Game::selectBase(int id)
{
	float x, y;
	mMgr->getHge()->Input_GetMousePos(&x, &y); 
	for(int i = 0; i < mPlayers[id].nrOfBuildings(); i++)
	{
		if( mPlayers[id].getBase(i)->inBase(x, y) )
			return mPlayers[id].getBase(i);
	}

	return NULL;
}
Base * Game::selectBase()
{
	float x, y;
	mMgr->getHge()->Input_GetMousePos(&x, &y); 

	for(size_t i = 0; i < mBases.size(); i++)
	{
		if( mBases[i]->inBase(x, y) )
		{
			return mBases[i];
		}
	}

	return NULL;
}
bool Game::inScreen(float x, float y, float rad)
{
	if(x-rad < 0)
		return false;
	if(x+rad > mWidth-150)
		return false;
	if(y-rad < 0)
		return false;
	if(y+rad > mHeight)
		return false;

	return true;
}
bool Game::buildBase(hgeColor col, float x, float y, int units, int id, int teamId, bool forceBuilt)
{
	if(inScreen(x, y, 20)==false)
		return false;

	//hgeColor tmp = mControl.getColor();
	Base * base;
	base = new Base(hgeColor(0, 0, 0, 1), x, y, 40, 20, id, teamId, mMgr);

	if (forceBuilt)
	{
		base->setSize(50);
		base->setRadius(50 + 44);
		base->forceBuilt();
		base->setOutColour(col);
		base->setColour(col);
	}
	base->setDstColour(col);

	mBases.push_back(base);

	return false;
}
void Game::updateUnitMovement(float dt)
{	
	for(size_t i = 0; i < mGroups.size(); i++)
	{
		if (!mGroups[i]->update(dt))
		{
			if (mHost)
				mClient->sendArrive(mGroups[i]);

			mFighting.push_back(mGroups[i]);
			mGroups.erase(mGroups.begin() + i);
			--i;
		}
	}
	if (mHost)
	{
		for(size_t i = 0; i < mFighting.size(); i++)
		{
			if (mFighting[i]->toRemove())
			{
				mClient->sendRmvGroup(mFighting[i]->getId(), mFighting[i]->getTeamId());
				mToRemove.push_back(mFighting[i]);
				mFighting.erase(mFighting.begin() + i);
				--i;
			}
		}
	}
}
void Game::initBases()
{
	hgeColor player = hgeColor(0.0f, 1.0f, 0.0f, 1.0f),
		     enemy  = hgeColor(0.0f, 1.0f, 0.0f, 1.0f);

	
	Base * b1 = new Base(player, 350, 250, 40, 50, -1, mPlayerID, mMgr);
	Base * b2 = new Base(enemy, 550, 450, 40, 50, -1, mEnemyID, mMgr);
	//b1 pos /*60, 60*/ b2 pos /*mWidth - 150 - 60, mHeight - 60*/


	b1->setDstColour(hgeColor(0.0f, 1.0f, 0.0f, 1.0f));
	b1->forceBuilt();
	//mBases.push_back(b1);

	b2->setDstColour(hgeColor(0.0f, 1.0f, 0.0f, 1.0f));
	b2->forceBuilt();

	mClient->sendNewBase(b1, -1);
	mClient->sendNewBase(b2, -1);
	
	//mBases.push_back(b2);
}
void Game::resetGame()
{

}

Base * Game::findBase(int id)
{
	for (int n = 0; n < mBases.size(); ++n)
	{
		if (mBases[n]->getId() == id)
			return mBases[n];
	}
	return 0;
}

Group * Game::findGroup(int id)
{
	for (int n = 0; n < mGroups.size(); ++n)
	{
		if (mGroups[n]->getId() == id)
			return mGroups[n];
	}
	for (int n = 0; n < mFighting.size(); ++n)
	{
		if (mFighting[n]->getId() == id)
			return mFighting[n];
	}
	for (int n = 0; n < mToRemove.size(); ++n)
	{
		if (mToRemove[n]->getId() == id)
			return mToRemove[n];
	}
	return 0;
}

Group * Game::removeGroup(int id)
{
	Group * rtn = 0;
	if (id < 0)
	{
		Group * g = findGroup(id);
		int h = 0;
	}
	for (int n = 0; n < mGroups.size(); ++n)
	{
		if (mGroups[n]->getId() == id)
		{
			rtn = mGroups[n];
			mGroups.erase(mGroups.begin() + n);
			break;
		}
	}
	for (int n = 0; n < mFighting.size(); ++n)
	{
		if (mFighting[n]->getId() == id)
		{
			rtn = mFighting[n];
			mFighting.erase(mFighting.begin() + n);
			break;
		}
	}
	for (int n = 0; n < mToRemove.size(); ++n)
	{
		if (mToRemove[n]->getId() == id)
		{
			rtn = mToRemove[n];
			mToRemove.erase(mToRemove.begin() + n);
			break;
		}
	}
	if (rtn != 0)
	{
		Base * b = rtn->getDest();
		b->removeGroup(rtn);
	}
	return rtn;
}

void Game::parseDestCol(std::stringstream &stream)
{
	int teamId, baseId;
	DWORD col;

	stream >> teamId >> baseId >> col;

	Base * b = findBase(baseId);
	if ( b == 0 ) 
		return;
	
	b->setDstColour(hgeColor(col));	
}

void Game::parseBase(std::stringstream &stream)
{
	int id, team;
	float x, y, radie, size;
	float units, advancement, time;
	DWORD col;

	stream >> team >> id >> x >> y >> col >> radie >> size >> units >> time >> advancement;

	// DO STUFF
	Base * b = findBase(id);
	if (b == 0)
		return;
	b->setRadius(radie);
	b->setSize(size);
	b->setUnits(units);
	b->setOutColour(col);
	b->setUnitTime(time);

	checkSelection();
	// TODO: DO MOAR
}
void Game::parseGroup(std::stringstream &stream)
{
	//sendGroup(int groupID, hgeColor colour, float nrOfUnits, float distanceMoved);
	int id;
	float nrOfUnits;
	hgeColor colour;
	DWORD col;
	float distMoved;

	stream >> id >> col >> nrOfUnits >> distMoved;
	//stream >> x >> y >> id >> col >> distMoved;
	colour.SetHWColor(col);

	// updatera group.
	Group * g = findGroup(id);
	if (g == 0)
		return;
	g->setNrOfUnits(nrOfUnits);
	g->setDistMoved(distMoved);

	// TODO: DO MOAR!!
}

void Game::parseSendMove(std::stringstream &stream)
{
	//sendMove(int groupID,int teamID, hgeColor colour, float toStop, float nrUnits, int origin, int dest);

	float x, y, toStop, nrOfUnits;
	int grpID, teamID, dest, origin;
	hgeColor colour;
	DWORD col;

	stream >> teamID;
	stream >> grpID;
	stream >> col;
	stream >> toStop;
	stream >> nrOfUnits;
	stream >> origin;
	stream >> dest;
	colour.SetHWColor(col);

	// Skicka enheter fr�n bas till annan.
	Base * o = findBase(origin);
	Base * d = findBase(dest);
	Group * g = new Group(d, o, nrOfUnits, grpID, teamID, mMgr);
	mGroups.push_back(g);
}

void Game::parseSendNewBase(std::stringstream &stream)
{
	//sendNewBase(float x, float y, int baseID, int teamID, hgeColor colour, bool forceBuilt);

	float x, y;
	int id, teamId, origin;
	bool force;
	hgeColor colour;

	DWORD col;

	stream >> teamId >> id >> x >> y >> col >> force;
	colour.SetHWColor(col);

	// Bygg ny bas.
	//hgeVector p;
	//hge->Input_GetMousePos(&p.x, &p.y);
	//buildBase(hgeColor(0.0f, 0.0f, 0.0f, 1.0f), x, y, 10);

	// hgeColor col, float x, float y, float inRadie, float size, int id, int teamId, Manager * mgr
	
	buildBase(colour, x, y, 0, id, teamId, force);
	/*Base * base = new Base(colour, x, y, 30, 100, id, teamId, mMgr);
	if (force)
		base->forceBuilt();

	mBases.push_back(base);*/
}
void Game::parseBaseCapture(std::stringstream &stream)
{
	//int baseID, int id
	int baseId, id, enemySize, attackersId, size;
	

	stream >> id >> baseId >> enemySize >> size;

	for ( int i = 0; i<size; ++i ) 
	{
		stream >> attackersId;
		Group * g = removeGroup(attackersId);
		if (g != 0)
			delete g;
	}
	// Ta �ver en bas.
	Base * base = findBase(baseId);
	base->capture(id, enemySize);

	checkSelection();
}

void Game::parseSacrifice(std::stringstream &stream)
{
	int baseId, id;
	float sacr;

	stream >> id >> baseId >> sacr;

	// Sacrafice gubbar och s�nt.
	
}

void Game::parseRmvGroup(std::stringstream &stream)
{
	int id, groupId;
	stream >> id >> groupId;

	Group * g = removeGroup(groupId);
	delete g;
}

void Game::parseArrive(std::stringstream &stream)
{
	int id, groupId, size, dest;
	DWORD col;
	hgeColor colour;
	stream >> id >> groupId >> size >> dest >> col;


	
	Group * g = findGroup(groupId);
	if (g == 0)
	{
		std::cout << "Could not find group " << groupId << ". ";
		colour.SetHWColor(col);	
		Base * b = findBase(dest);
		if (b != 0)
		{
			g = new Group();
			g->restore(b, size, groupId, id, colour, mMgr);
			b->addGroup(g);
		}
		else
			std::cout << "Failed to recreate group " << groupId << ".";
		std::cout << '\n';
		return;
	}
	Base * b = g->getDest();
	b->addGroup(g);
}

void Game::amount()
{
	mArea2 = 0;
	mArea1 = 0;
	int		i = 0;

	while((size_t)i < (size_t)mBases.size())
	{
		if(mBases.at(i)->getTeamId() == 0)
		{
			mArea1 += mBases.at(i)->getRadius();
		}
		else if(mBases.at(i)->getTeamId() == 1)
			mArea2 += mBases.at(i)->getRadius();
		++i;
	}
}

void Game::won()
{
	int win = -1;

	if(mArea1 == 0 && mArea2 != 0)
	{
		mServer->end();
		win = 1;
		
	}
	if(mArea2 == 0 && mArea1 != 0)
	{
		mServer->end();
		win = 0;
	}
	if(win != -1 && mPlayerID == 0)
	{
		win = !win;
	}

	if (win == 1)
		mMgr->getSceneMgr()->changeScene("WinScene",1);
	else if(win == 0)
		mMgr->getSceneMgr()->changeScene("LoseScene",1);

}

void Game::multiSelect(std::vector<Base *> b, hgeVector p1, hgeVector p2)
{
	if (p1.x > p2.x )
		std::swap(p1.x, p2.x);		
	if ( p1.y > p2.y )
		std::swap(p1.y, p2.y);

	bool removeBack = false;
	if (!mSelection.empty())
	{
		removeBack = !(mSelection.back()->getTeamId() == mPlayerID);
	}

	for ( int i = 0; i< b.size(); ++i )
	{
		if ( b.at(i)->checkSelection(p1, p2) && b.at(i)->getTeamId() == mPlayerID)
		{
			if (removeBack)
			{
				mSelection.back()->unselect();
				mSelection.pop_back();
				removeBack = false;
			}
			mSelection.push_back(b.at(i));
			mSelection.back()->select();
		}
	}
}

	
void Game::playSound(std::string desc)
{
	mMgr->getSoundMgr()->startSound(desc, S_EFFECT);
}

Base * Game::getBuilder(hgeVector pos)
{
	Base * closest = mSelection[0];
	float dist = (mSelection[0]->getPos() - pos).Length();
	float tDist;
	for (int n = 1; n < mSelection.size(); ++n)
	{
		tDist = (mSelection[n]->getPos() - pos).Length();
		if (tDist < dist && (mSelection[n]->getNrOfUnits() >= 10 || closest->getNrOfUnits() < 10))
		{
			dist = tDist;
			closest = mSelection[n];
		}
	}

	return closest;
}

void Game::renderSRect()
{
	hgeColor temp(0, 0.75, 0, 1);
	mMgr->getHge()->Gfx_RenderLine(mOldMousePos.x, mOldMousePos.y, mOldMousePos.x, mMousePos.y, temp.GetHWColor(), 0.5);
	mMgr->getHge()->Gfx_RenderLine(mOldMousePos.x, mOldMousePos.y, mMousePos.x, mOldMousePos.y, temp.GetHWColor(), 0.5);
	mMgr->getHge()->Gfx_RenderLine(mMousePos.x, mMousePos.y, mOldMousePos.x, mMousePos.y, temp.GetHWColor(), 0.5);
	mMgr->getHge()->Gfx_RenderLine(mMousePos.x, mMousePos.y, mMousePos.x, mOldMousePos.y, temp.GetHWColor(), 0.5);
}

void Game::checkSelection()
{
	for (int n = 0; n < mSelection.size(); ++n)
	{
		if (mSelection[n]->getTeamId() != mPlayerID)
		{
			mSelection[n]->unselect();
			mSelection.erase(mSelection.begin() + n);
			--n;
		}
	}
}