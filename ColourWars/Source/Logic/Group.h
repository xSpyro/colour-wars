#ifndef GROUP_H
#define GROUP_H

#include "../Manager/Manager.h"
#include <hgecolor.h>
#include <hgevector.h>
#include <hgesprite.h>
#include <hgefont.h>

class Base;

class Group
{
public:

	Group();
	Group(Base * dest, Base * origin, float nrUnits, int id, int teamId, Manager * mgr);
	virtual ~Group();


	bool update(float dt);
	void init(Base * dest, Base * origin, float nrUnits, int id, int teamId, Manager * mgr);
	void render();

	void restore(Base * dest, int units, int id, int teamId, hgeColor col, Manager * mgr);

	int getId();
	Base * getDest();
	void setId(int id);

	//Network Communicationstuff
	std::string msgGroup();
	std::string msgMove();
	std::string msgArrive();

	int getTeamId();

	void setDistMoved(float dist);

	void setColor(hgeColor color)
	{
		mColor = color;
	}
	void setPos(hgeVector pos)
	{
		mPosition = pos;
	}

	hgeVector getPos()
	{
		return mPosition;
	}

	hgeColor getColour()
	{
		return mColor;
	}

	float getNrOfUnits()
	{
		return mNrOfUnits;
	}

	bool lose(float units)
	{
		mNrOfUnits -= units;
		if (mNrOfUnits <= 0)
		{
			mRemove = true;
			return false;
		}
		return true;
	}
	void addNrOfUnits(float nr)
	{
		mNrOfUnits += nr;
	}
	void setNrOfUnits(float nr)
	{
		mNrOfUnits = nr;
		if (nr > 0)
			mRemove = false;
		else
			mRemove = true;
	}

	bool toRemove()
	{
		return mRemove;
	}

private:
	Manager * mMgr;

	hgeSprite * mSprite;
	hgeSprite * mRing;
	hgeFont * mFont;
	hgeColor mTextCol;
	float mTextScale;
	
	int mId;
	int mTeamID;
	hgeColor mColor;
	hgeVector mPosition;

	Base * mOrigin;
	Base * mDestination;

	float mNrOfUnits;

	// Size of the visual texture.
	float mSize;
	float mSpeed;

	hgeVector mDir;

	float mDistMoved;
	float mToStop;

	bool mRemove;
};

#endif
