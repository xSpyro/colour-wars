#include "../Network/Client.h"
#include "Base.h"
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <float.h>

Base::Base() : mNrOfUnits(0), mAdvancement(0), mMaxRatio(1.5f), mUnitTime(0), mSpeed(100), mBaseChange(true), mTriChange(true), mMode(B_FIELD), mBuilt(false), mSelected(false)
{
	mAdvancement = 0;
	mSpeed = 1.0f;
	mInRadie = 1.0f;
	mSize = 2.0f;

	mID     = -1;
	mTeamID = -1;

	mMaxRatio = 1.5f;

	mUnitTime = 0.5f;
}   

Base::Base(hgeColor col, float x, float y, float inRadie, float size, int id, int teamId, Manager * mgr) 
: mNrOfUnits(0), mAdvancement(0), mMaxRatio(1.5f), mUnitTime(0), mSpeed(100), mOutCol(col), mBaseChange(true), mTriChange(true), mMode(B_FIELD), mBuilt(false), mSelected(false)
{
	init(col, x, y, inRadie, size, id, teamId, mgr);

	if (_isnan(mOutCol.r))
		int h = 0;

	//mAdvancement = 50;
}

Base::~Base()
{}

bool Base::update(float dt)
{ 
	if (!mBuilt)
		return true;

	if (_isnan(mOutCol.r))
		int h = 0;

	if(mAdvancement > 0 || mSize < 50 || mOutCol.GetHWColor() != mDstCol.GetHWColor() )
	{
		float mAdd = dt * 1;
		float extra = 0;

		if(mAdvancement > 0)
		{
			extra = dt * 9;
			if (extra > mAdvancement)
				extra = mAdvancement;
			mAdvancement -= extra;
			mAdd += extra;
		}

		if (mSize > 200 && mRad > 150)
			mAdd *= 1.5;
		else if (extra != 0 || mSize < 50)
		{
			float add = mAdd;
			if (mSize > 50)
				add *= (4.0/3.0) - (mSize/150);
			if (mSize > 200)
				add = 0;
			mSize += add;

			if (mRad < 150)
				mRad += add;
		}

		mAdd *= (1.0/255.0) * 20.0;

		hgeColor mDir = mDstCol - mOutCol;
		hgeColor tDebug = mOutCol;

		float size = sqrt((mDir.r * mDir.r) + (mDir.g * mDir.g) + (mDir.b * mDir.b));

		if (size != 0)
		{
			size = 1 / size;
			if (_isnan(size))
				int h = 0;

			mDir.r *= size;
			mDir.g *= size;
			mDir.b *= size;

			mOutCol += mDir * mAdd;

			mOutCol.Clamp();

			updateTriVert();

			mBaseChange = true;
		}
	}
	
	if(!mAttackers.empty())
	{
		fight(dt);
	}
	//else
	{
		if(mNrOfUnits < (int)mSize)
		{
			// Spawn unit.
			mUnitTime -= dt;
			while(mUnitTime < 0)
			{
				mNrOfUnits++;
				mUnitTime += 0.75f;// (mSize / (float)mNrOfUnits) * 2;
			}
		}
		else if (mNrOfUnits > (int)(mSize * mMaxRatio))
		{
			mUnitTime -= dt;
			while(mUnitTime < 0)
			{
				if (mNrOfUnits <= 1)
					mNrOfUnits = 2;

				--mNrOfUnits;
				mUnitTime += ((mSize * mMaxRatio) /((float)mNrOfUnits)) * 0.1f;
			}
		}
	}

	return true;
}

int Base::getSize()
{
	return mSize;
}


void Base::init(hgeColor col, float x, float y, float inRadie, float size, int id, int teamId, Manager * mgr)
{
	mMgr = mgr;

	mFont = mMgr->getFontMgr()->getFont("Font/base.fnt", "base");

	mOutCol = mCol = mDstCol = col;
	mInRadie = inRadie;
	mAdvancement = size;
	mSize = 0;
	mRad = 44;
	mID = id;

	mTeamID = mNewTeam = teamId;

	mX = x;
	mY = y;

	mBaseChange = true;
	mTriChange = true;

	setTri();
	setQuad();
	
	mBase = new hgeSprite(mMgr->getTexMgr()->getTexture("Image/base.png"), -15, -15, 30, 30);
	mBase->SetBlendMode(BLEND_COLORMUL | BLEND_ALPHABLEND);
	mBase->SetHotSpot(15, 15);
	mBase->SetTextureRect(0, 0, 128, 128, false);
	mBase->SetColor(mCol.GetHWColor());

	mRing = new hgeSprite(mMgr->getTexMgr()->getTexture("Image/base.png"), -20, -20, 40, 40);
	mRing->SetBlendMode(BLEND_COLORMUL | BLEND_ALPHABLEND);
	mRing->SetHotSpot(20, 20);
	mRing->SetTextureRect(0, 0, 128, 128, false);
	mRing->SetColor(mMgr->getTeamColour(mTeamID).GetHWColor());

	mDest = new hgeSprite(mMgr->getTexMgr()->getTexture("Image/Slice2.png"), -20, -20, 40, 40);
	mDest->SetHotSpot(20, 20);
	mDest->SetTextureRect(0, 0, 128, 128, false);
	mDest->SetColor(mDstCol.GetHWColor());

	mOut = new hgeSprite(mMgr->getTexMgr()->getTexture("Image/Slice2.png"), -20, -20, 40, 40);
	mOut->SetHotSpot(20, 20);
	mOut->SetTextureRect(0, 128, 128, -128, false);
	mOut->SetColor(mOutCol.GetHWColor());
}
void Base::addGroup(Group * group)
{
	if(group->getTeamId() == mTeamID)
	{
		if (!mBuilt)
		{
			mBuilt = true;
			if (group->lose(10) && group->getNrOfUnits() < 0)
				mBuilt = false;
		}

		if (mBuilt)
		{
			mNrOfUnits += group->getNrOfUnits();
			group->setNrOfUnits(0);
		}
	}
	else
	{
		if (mBuilt)
			mAttackers.push_back(group);
		else
			delete group;
	}
}
void Base::setDstColour(hgeColor dst)
{
	mDstCol = dst;
	mDest->SetColor(mDstCol.GetHWColor());
}
/**
 * Implementation needed.
 */
void Base::fight(float dt)
{
	float attSize = enemySize();
	float speed = (attSize + mNrOfUnits) / 20;
	if (speed < 1)
		speed = 1;
	if (attSize <= 0 )
		return;

	for (UINT n = 0; n < mAttackers.size(); ++n)
	{
		hgeColor attCol = mAttackers[n]->getColour();
		hgeColor att;
		hgeColor def;
		float ratio = mAttackers[n]->getNrOfUnits() / attSize;

		float a = 1;
		float d = 1;

		def.r = 1 + mCol.g - attCol.r;
		def.g = 1 + mCol.b - attCol.g;
		def.b = 1 + mCol.r - attCol.b;

		att.r = 1 + attCol.g - mCol.r;
		att.g = 1 + attCol.b - mCol.g;
		att.b = 1 + attCol.r - mCol.b;

		hgeColor one(1, 1, 1, 0);

		att = att * att - one;
		def = def * def - one;
		att *= 0.5;
		def *= 0.5;

		if (def.r > 0)
			d += def.r;
		if (def.g > 0)
			d += def.g;
		if (def.b > 0)
			d += def.b;

		if (att.r > 0)
			a += att.r;
		if (att.g > 0)
			a += att.g;
		if (def.b > 0)
			a += att.b;

		mNrOfUnits -= a * ratio * dt * 2 * speed;
		if (!mAttackers[n]->lose(d * ratio * dt * 2 * speed))
		{
			if (mNrOfUnits < mAttackers[n]->getNrOfUnits())
			{
				mAttackers[n]->setNrOfUnits(0);
				mAttackers.erase(mAttackers.begin() + n);
				n--;
			}
			else
			{
				mAttackers.erase(mAttackers.begin() + n);
				n--;
			}
		}
		else if (mNrOfUnits <= 0)
			mNewTeam = mAttackers[n]->getTeamId();
	}
}

void Base::capture(int id, int size)
{
	//mID = id;
	mTeamID = id;
	mNewTeam = id;

	mNrOfUnits = size;

	mBaseChange = true;
	mTriChange = true;
	updateQuad();
	updateTri();

	if (mSelected)
	{
		hgeColor temp(mMgr->getTeamColour(mTeamID));
		temp += hgeColor(0.5, 0.5, 0.5, 1);
		temp.Clamp();
		mRing->SetColor(temp.GetHWColor());
	}
	else
		mRing->SetColor(mMgr->getTeamColour(mTeamID).GetHWColor());
}

Group * Base::sendGroupTo(Base * dest, int units, int id, int teamId, bool doNothing)
{
	float size = 0;
	if(units > mNrOfUnits)
	{
		size = mNrOfUnits;
		if (!doNothing)
			mNrOfUnits = 0;
	}	
	else
	{
		size = units;
		if (!doNothing)
			mNrOfUnits -= units;
	}

	Group * gr;
	gr = new Group(dest, this, size, id, teamId, mMgr);

	return gr;
}

bool Base::buildBase(hgeColor col, float x, float y, int units, int id, Group ** gOut, Base ** bOut, bool doNothing)
{
	if (units < 10 || mNrOfUnits < 10)
		return false;

	*bOut = new Base(col, x, y, 40, 100, id, mTeamID, mMgr);
	*gOut = sendGroupTo(*bOut, 10, -1, mTeamID, doNothing); // REMOVE TRUE LATER? NOW IT DOESN'T REMOVE THE UNITS!!!

	return true;
}

float Base::influence(Base &b)
{
	float dist = distance(b);
	hgeColor temp;
	if (dist < mRad)
	{
		float r = ((mRad - dist) / mRad);

		b.mCol += mOutCol * r;
		b.mCol.a = 1;

		b.mCol.Clamp();
		//b.mCol = (1 - r) * b.mCol + r * mCol;
		//b.mCol += mOutCol * (dist / mRad);
		b.mBaseChange = true;
	}
	if (dist < b.mRad)
	{
		float r = ((b.mRad - dist) / b.mRad);

		mCol += b.mOutCol * r;
		mCol.a = 1;

		mCol.Clamp();
		//mCol = (1 - r) * mCol + r * b.mCol;
		//mCol += b.mOutCol * (dist / b.mRad);
		mBaseChange = true;
	}
	if (dist < mRad + b.mRad && mTeamID != b.mTeamID)
	{
		return dist;
	}
	return -1;
}

float Base::distance(Base &b)
{
	return sqrt((mX - b.mX) * (mX - b.mX) + (mY - b.mY) * (mY - b.mY));
}

float Base::distance(hgeVector p)
{
	return sqrt((mX - p.x) * (mX - p.x) + (mY - p.y) * (mY - p.y));
}

hgeVector Base::getDivider(Base &b, float dist)
{
	hgeVector distVec = hgeVector(b.mX, b.mY) - hgeVector(mX, mY); 
	hgeVector dir = distVec;
	if (dist < 0)
	{
		dir.Normalize();
	}
	else
	{
		dir *= (1 / dist);
	}

	hgeVector rtn = (dir * mRad + distVec - dir * b.mRad) * 0.5 + hgeVector(mX, mY);

	return rtn;
}


float Base::getAlphaAt(hgeVector p)
{
	float rtn = 1 - (distance(p) / mRad);
	if (rtn > 1)
		return 1;
	if (rtn < 0)
		return 0;
	return rtn;
}

void Base::renderBase()
{
	mOut->SetColor(mOutCol.GetHWColor());

	mRing->Render(mX, mY);
	mDest->Render(mX, mY);
	mOut->Render(mX, mY);
	mBase->Render(mX, mY);

	// hgeColor temp = hgeColor(1, 1, 1, 1) - mCol;
	hgeColorHSV tCol(mCol.GetHWColor());
	hgeColorHSV temp = hgeColorHSV(1, 1, 1, 1) - tCol;
	temp.a = 1;
	mFont->SetColor(temp.GetHWColor());
	if (!mAttackers.empty())
		mFont->printf(mX, mY - 10, HGETEXT_CENTER, "%i-%i", getNrOfUnits(), (int)enemySize());
	else
		mFont->printf(mX, mY - 10, HGETEXT_CENTER, "%i", getNrOfUnits());
}

void Base::renderField(BaseDisplay mode)
{
	if (mMode != mode)
	{
		mMode = mode;
		updateTriVert();
	}
	HGE * hge = mMgr->getHge();
	for (int n = 0; n < CIRCLE_CORNERS; ++n)
	{
		hge->Gfx_RenderTriple(&mTri[n]);
	}
}

void Base::updateTri()
{
	if (mTriChange)
	{
		mOutCol.Clamp();

		hgeColor border;
		hgeColor center;
		switch (mMode)
		{
		case B_FIELD:
			center = mOutCol;
			border = mOutCol;
			break;
		case B_TEAM:
			switch (mTeamID)
			{
			case 0:
			case 2:
				center = mMgr->getTeamColour(mID);
				break;
			case 1:
				center = mMgr->getTeamColour(mID);
				break;
			}
			border = center;
			break;
		}
		border.a = 0;

		for (int n = 0; n < CIRCLE_CORNERS; ++n)
		{
			mTri[n].v[0].col = center.GetHWColor();
			mTri[n].v[1].col = border.GetHWColor();
			mTri[n].v[2].col = border.GetHWColor();
		}
		
		mTriChange = false;
	}
}

void Base::updateQuad()
{
	if (mBaseChange)
	{
		mCol.Clamp();

		mBase->SetColor(mCol.GetHWColor());

		mBaseChange = false;
	}
}

void Base::setMode(BaseDisplay mode)
{
	mMode = mode;
	mTriChange = true;
	updateTri();
}

void Base::setQuad()
{
	mCol.Clamp();
}

void Base::setTri()
{
	hgeColor border;
	hgeColor center;
	switch (mMode)
	{
	case B_FIELD:
		center = mOutCol;
		border = mOutCol;
		break;
	case B_TEAM:
		switch (mTeamID)
		{
		case 0:
		case 2:
			center = hgeColor(0, 0, 1, 1);
			break;
		case 1:
			center = hgeColor(1, 0, 0, 1);
			break;
		}
		border = center;
		break;
	}
	border.a = 0; // 0x00ffffff border.GetHWColor()
	hgeTriple temp = 
	{
		{
			{mX, mY, 0.5f, center.GetHWColor(), 0, 0},
			{0, 0, 0.5f, border.GetHWColor(), 1, 0},
			{0, 0, 0.5f, border.GetHWColor(), 0, 1}
		},
			0,
			BLEND_COLORMUL | BLEND_ALPHAADD
	}; 

	float slice = 3.14159265358979 * 2 / CIRCLE_CORNERS;
	for (int n = 0; n < CIRCLE_CORNERS; ++n)
	{
		temp.v[1].x = cos(n * slice) * mRad + mX;
		temp.v[1].y = sin(n * slice) * mRad + mY;

		temp.v[2].x = cos((n + 1) * slice) * mRad + mX;
		temp.v[2].y = sin((n + 1) * slice) * mRad + mY;

		mTri[n] = temp;
	}
}

void Base::setColour(hgeColor col)
{
	mCol = col;
	mBaseChange = true;
	mTriChange = true;
}

void Base::setOutColour(hgeColor col)
{
	mOutCol = col;
	mTriChange = true;
}

void Base::setPosition(float x, float y)
{
	mX = x;
	mY = y;

	updateTriVert();
}

void Base::setRadius(float r) // Often mRad should be 44 bigger then mSize.
{
	mRad = r;
	updateTriVert();
}

void Base::resetColour()
{
	mCol = mOutCol;
}

void Base::setManger(Manager * mgr)
{
	mMgr = mgr;
}

void Base::updateTriVert()
{
	hgeColor border;
	hgeColor center;
	switch (mMode)
	{
	case B_FIELD:
		center = mOutCol;
		border = mOutCol;
		break;
	case B_TEAM:
		switch (mTeamID)
		{
		case 0:
		case 2:
			center = hgeColor(0, 0, 1, 1);
			break;
		case 1:
			center = hgeColor(1, 0, 0, 1);
			break;
		}
		border = center;
		break;
	}
	border.a = 0; // 0x00ffffff border.GetHWColor()
	hgeTriple temp = 
	{
		{
			{mX, mY, 0.5f, center.GetHWColor(), 0, 0},
			{0, 0, 0.5f, border.GetHWColor(), 1, 0},
			{0, 0, 0.5f, border.GetHWColor(), 0, 1}
		},
			0,
			BLEND_COLORMUL | BLEND_ALPHAADD
	}; 

	float slice = 3.14159265358979 * 2 / CIRCLE_CORNERS;
	for (int n = 0; n < CIRCLE_CORNERS; ++n)
	{
		temp.v[1].x = cos(n * slice) * mRad + mX;
		temp.v[1].y = sin(n * slice) * mRad + mY;

		temp.v[2].x = cos((n + 1) * slice) * mRad + mX;
		temp.v[2].y = sin((n + 1) * slice) * mRad + mY;

		mTri[n] = temp;
	}
}

hgeColor Base::getColour()
{
	return mCol;
}

hgeColor Base::getOutColour()
{
	return mOutCol;
}

float Base::getRadius()
{
	return mRad;
}

void Base::forceBuilt()
{
	mBuilt = true;
}

bool Base::inBase(float x, float y)
{
	x = mX-x;
	y = mY-y;
	float size = sqrt(x*x+y*y);
	if(size < mRing->GetWidth() * 0.5)
		return true;
	return false;
}



std::string Base::msgBase()
{
	std::stringstream ss;
	ss << MSG_BASE << " ";
	ss << mTeamID << " ";
	ss << mID << " ";
	ss << mX << " ";
	ss << mY << " ";
	ss << mOutCol.GetHWColor() << " ";
	ss << mRad << " ";
	ss << mSize << " ";
	ss << mNrOfUnits << " ";
	ss << mUnitTime << " ";
	ss << mAdvancement << " ";

	std::string temp;
	getline(ss, temp, '\n');
	return temp;
}


std::string Base::msgNew(int origin)
{
	std::stringstream ss;
	ss << MSG_NEWBASE << " ";
	ss << mTeamID << " ";
	ss << mID << " ";
	ss << mX << " ";
	ss << mY << " ";
	ss << mDstCol.GetHWColor() << " ";	
	ss << mBuilt << " ";
	ss << origin << " ";

	std::string temp;
	getline(ss, temp, '\n');
	return temp;
}

std::string Base::msgSetDestCol()
{
	std::stringstream ss;
	ss << MSG_SETDESTCOL << " ";
	ss << mTeamID << " ";
	ss << mID << " ";
	ss << mDstCol.GetHWColor() << " ";
	
	std::string temp;
	getline(ss, temp, '\n');
	return temp;
}

std::string Base::msgCaptured()
{
	int size = -1;
	std::vector <int> t;
	std::stringstream ss;
	ss << MSG_BASECAPTURE << " ";
	ss << mNewTeam << " ";
	ss << mID << " ";
	ss << (int)enemySize() << " ";

	t = getAttackersId();
	size = t.size();
	ss << size << " ";
	for ( int i = 0; i < size; ++i ) 
		ss << t[i] << " ";

	std::string temp;
	getline(ss, temp, '\n');
	return temp;
}

void Base::setUnitTime(float time)
{
	mUnitTime = time;
}

bool Base::isCaptured()
{
	return (mNewTeam != mTeamID);
}

bool Base::checkSelection(hgeVector p1, hgeVector p2)
{
	if (p1.x > p2.x )
		std::swap(p1.x, p2.x);		
	if ( p1.y > p2.y )
		std::swap(p1.y, p2.y);

	if ( mX > p1.x && mY > p1.y && mX < p2.x && mY < p2.y )
		return true;
	return false;
}

void Base::removeGroup(Group * group)
{
	for (int n = 0; n < mAttackers.size(); ++n)
	{
		if (mAttackers[n] == group)
		{
			mAttackers.erase(mAttackers.begin() + n);
			--n;
		}
	}
}

void Base::sacrifice(int units)
{
	if (units > mNrOfUnits)
		units = mNrOfUnits;
	if (units <= 0)
		return;

	mAdvancement += units;
	mNrOfUnits -= units;
}

void Base::select()
{
	hgeColor temp(mMgr->getTeamColour(mTeamID));
	temp += hgeColor(0.5, 0.5, 0.5, 1);
	temp.Clamp();
	mRing->SetColor(temp.GetHWColor());

	mSelected = true;
}

void Base::unselect()
{
	mRing->SetColor(mMgr->getTeamColour(mTeamID).GetHWColor());
	mSelected = false;
}

void Base::setSize(float size)
{
	mSize = size;
}

std::vector<int> Base::getAttackersId()
{
	std::vector<int> rtn;

	for (int n = 0; n < mAttackers.size(); ++n)
	{
		if (_isnan(mAttackers[n]->getNrOfUnits()))
			int h = 0;
		rtn.push_back(mAttackers[n]->getId());
	}
	return rtn;
}