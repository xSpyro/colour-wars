#include "Grid.h"

Grid::Grid()
{

}
Grid::~Grid()
{

}
void Grid::init(int level, int w, int h)
{	
	hgeVector pos = hgeVector(0, 0);
	float stepX = (float)(w/level),
		  stepY = (float)(h/level);

	// r-rows, c-cols
	for(int r = 0; r < level; r++)
	{
		for(int c = 0; c < level; c++)
		{
			addNode(pos, stepX, stepY);

			pos.x += stepX;
		}

		pos.x = 0;
		pos.y += stepY;
	}


}
void Grid::addObject(Base * base)
{

}
std::vector<Base*> Grid::collide(float x, float y)
{
	// Loop nodes for collision, if, collision found
	// test for collision all object in the node.
	for(size_t n = 0; n < mNodes.size(); n++)
	{
		if( inNode(n,x, y) )
		{
			
			return mNodes[n].bases;
		}
	}

	std::vector<Base*> empty;
	return empty;
}
void Grid::addNode(hgeVector pos, float w, float h)
{
	Node node;
	
	node.w = w;
	node.h = h;
	
	node.pos = pos;

	mNodes.push_back(node);
	
}
bool Grid::inNode(int id, float x, float y)
{
	if( x < mNodes[id].pos.x )
		return false;
	if( x > mNodes[id].pos.x+mNodes[id].w )
		return false;
	if( y < mNodes[id].pos.y )
		return false;
	if( y > mNodes[id].pos.y+mNodes[id].h )
		return false;

	return true;
}
Base * Grid::objectInNode(int id, float x, float y)
{
	for(size_t i = 0; i < mNodes[id].bases.size(); i++)
	{
		if( mNodes[id].bases[i]->inRadius(x, y) )
		{
			return mNodes[id].bases[i];
		}
	}

	return NULL;
}




