#ifndef GRID_H
#define GRID_H

#include "Base.h"

class Grid
{

public:
	Grid();
	virtual ~Grid();

	// level 1, for no splitups.
	void init(int level, int w, int h); 

	void addObject(Base * base);

	// Return base, that collides with xy position.
	std::vector<Base*> collide(float x, float y);



private:
	
	struct Node
	{
		std::vector< Base * > bases;
		float w;
		float h;
		hgeVector pos;
	};	

private:


	void addNode(hgeVector pos, float w, float h);

	// If xy is in node.
	bool inNode(int id, float x, float y);

	// If xy collides with an object in node at id.
	Base * objectInNode(int id, float x, float y);
	

	std::vector<Node> mNodes;



};


#endif