#include "SceneTestPrg.h"
#include "Logic/Game.h"
#include "gui/MenuScenes/MainMenu.h"
#include "gui/MenuScenes/JoinGameMenu.h"
#include "gui/MenuScenes/EndMenuScene.h"
#include "gui/MenuScenes/LoseScene.h"
#include "gui/MenuScenes/WinScene.h"
#include "gui/MenuScenes/HelpScene.h"

SceneTest::SceneTest()
{
}

SceneTest::~SceneTest()
{}

bool SceneTest::initHge(HGE *hge)
{
	hge->System_SetState(HGE_WINDOWED, true);
	hge->System_SetState(HGE_SCREENWIDTH, 1024);
	hge->System_SetState(HGE_SCREENHEIGHT, 768);
	hge->System_SetState(HGE_SCREENBPP, 32);
	hge->System_SetState(HGE_HIDEMOUSE, false);
	hge->System_SetState(HGE_DONTSUSPEND, true);
	hge->System_SetState(HGE_TITLE, "Colour Wars, take those BASES!!!"); // TODO: Change?

	return Program::initHge(hge);
}

bool SceneTest::initPrg()
{
	SceneMgr * sMgr = mMgr->getSceneMgr();

	sMgr->addScene("MainMenu", new MainMenu(mMgr));
	sMgr->addScene("JoinMenu", new JoinGameMenu(mMgr));
	sMgr->addScene("EndMenu", new EndMenuScene(mMgr));
	sMgr->addScene("WinScene", new WinScene(mMgr));
	sMgr->addScene("LoseScene", new LoseScene(mMgr));
	sMgr->addScene("HelpScene", new HelpScene(mMgr));

	sMgr->changeScene("MainMenu");

	float ScWidth = (float)mMgr->getHge()->System_GetState(HGE_SCREENWIDTH);
	float ScHeight = (float)mMgr->getHge()->System_GetState(HGE_SCREENHEIGHT);

	hgeFont *temp;
	temp = (mMgr->getFontMgr()->getFont("Font/Text20.fnt"));

	mMgr->getHge()->Gfx_BeginScene();
	temp->printf(ScWidth / 2, ScHeight / 2, HGETEXT_CENTER, "Loading sounds");
	mMgr->getHge()->Gfx_EndScene();

	mMgr->getSoundMgr()->init(mMgr->getHge());
	mMgr->getMusicList()->init(mMgr->getSoundMgr());
	mMgr->getMusicList()->loadDefault();
	mMgr->getMusicList()->setVolume(60);

	return false;
}

bool SceneTest::update(float dt)
{
	mMgr->getMusicList()->update();
	if(mMgr->getHge()->Input_KeyDown('M'))
		mMgr->getMusicList()->stop();
	if(mMgr->getHge()->Input_KeyDown(HGEK_I))
		mMgr->getMusicList()->increaseVol();
	if(mMgr->getHge()->Input_KeyDown(HGEK_L))
		mMgr->getMusicList()->decreaseVol();
	if(mMgr->getHge()->Input_KeyDown(HGEK_P))
		mMgr->getMusicList()->prev();
	if(mMgr->getHge()->Input_KeyDown(HGEK_N))
		mMgr->getMusicList()->next();


	HGE * hge = mMgr->getHge();

	if (hge->Input_GetKeyState('B') && hge->Input_GetKeyState('A') && hge->Input_GetKeyState('S') && hge->Input_KeyDown('E'))
	{
		mMgr->getMusicList()->playSecret();
	}

	return mMgr->getSceneMgr()->update(dt);
}

void SceneTest::render()
{
	mMgr->getSceneMgr()->render();
}

bool SceneTest::restore()
{
	return mMgr->getSceneMgr()->restore();
}

void SceneTest::terminate()
{}