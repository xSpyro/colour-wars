#ifndef SCENE_H
#define SCENE_H

#include "Manager/Manager.h"

class Scene
{
public:
	Scene();
	Scene(Manager *tMgr);
	virtual void init(Manager * tMgr);
	virtual bool update(float dt) = 0;
	virtual void render() = 0;
	virtual bool restore();
	virtual void onPause();
	virtual void onResume();
	void pause();
	void resume();
	virtual void enterScene();
	virtual void exitScene();

protected:
	Manager *mMgr;
	bool	mPaused; // Remove?
};
#endif