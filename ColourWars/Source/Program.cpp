#include "Program.h"

Program::Program()
{}

Program::~Program()
{}

bool Program::initHge(HGE * hge)
{
	mMgr = new Manager(hge);

	mSounds.init(hge);

	return true;
}

bool Program::initPrg()
{
	return true;
}

bool Program::restore()
{
	return true;
}

void Program::terminate()
{
	mSounds.terminate();
}

bool Program::hgeUpdate()
{
	float dt = mMgr->getHge()->Timer_GetDelta();

	return !update(dt);
}

bool Program::hgeRender()
{
	render();

	return false;
}

bool Program::hgeRestore()
{
	return !restore();
}