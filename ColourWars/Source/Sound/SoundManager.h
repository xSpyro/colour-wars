#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include <hge.h>
#include <vector>
#include <string>

enum SoundType
{
	S_EFFECT	= 1,
	S_MUSIC		= 2,
	S_DEFAULT	= -1
};

struct Sound
{
	Sound()
	{
		type = S_DEFAULT;
		id = -1;
	}
	Sound(SoundType tType, int tId)
	{
		type = tType;
		id = tId;
	}
	SoundType type;
	int id;
	std::string desc;
};

class SoundManager
{
public:
	SoundManager();
	SoundManager(HGE *hge);

	virtual bool init(HGE * hge);
	virtual bool update(float dt);
	virtual void terminate();

	std::string addSound(std::string name, SoundType type, std::string desc);
	bool startSound(Sound temp);
	bool startSound(std::string desc, SoundType type);

	void silence(SoundType type);
	void silenceAll();
	bool checkMusicSilence();

	void setVolume(int v);


private:
	HGE * mHge;
	HCHANNEL	mEffectsCh;
	HCHANNEL	mMusicCh;

	HEFFECT snd;

	std::vector<Sound>		mMusicList;
	std::vector<Sound>		mEffectList;
	std::vector<HMUSIC>		mMusic;
	std::vector<HEFFECT>	mEffects;
	HMUSIC	mSecret;
};


#endif;