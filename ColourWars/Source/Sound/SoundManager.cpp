#include "SoundManager.h"

SoundManager::SoundManager()
{
	mSecret = 0;
}

SoundManager::SoundManager(HGE *hge)
{
	mHge = hge;

	mSecret = mHge->Effect_Load("Sound/AllBases.mp3");
}

bool SoundManager::init(HGE *hge)
{
	mHge = hge;
	if (mSecret == 0)
	{
		mSecret = mHge->Effect_Load("Sound/AllBases.mp3");
		if (mSecret == 0)
			return false;
	}
	return true;
}

bool SoundManager::update(float dt)
{
	return true;
}

void SoundManager::terminate()
{
	for(size_t i=0; i<mEffects.size(); ++i)
		mHge->Effect_Free(mEffects.at(i));
	for(size_t i=0; i<mMusic.size(); ++i)
		mHge->Effect_Free(mMusic.at(i));
}

std::string SoundManager::addSound(std::string name, SoundType type, std::string desc)
{
	Sound temp;
	temp.id = -1;
	temp.type = type;
	temp.desc = desc;
	//Effect
	if(type == S_EFFECT)
	{

		if(!mHge->Effect_Load(name.c_str()))
			return "ERROR";

		temp.id = mEffects.size();
		mEffects.push_back(mHge->Effect_Load(name.c_str()));
		mEffectList.push_back(temp);
	}
	//music
	else if(type == S_MUSIC)
	{

		if(!mHge->Effect_Load(name.c_str()))
			return "ERROR";

		temp.id = mMusic.size();
		mMusic.push_back(mHge->Effect_Load(name.c_str()));
		mMusicList.push_back(temp);
	}

	return temp.desc;
}

bool SoundManager::startSound(Sound temp)
{
	//effect
	if(temp.type == S_EFFECT)
	{
		if((size_t)temp.id < (size_t)0 || (size_t)temp.id > (size_t)mEffects.size())
			return false;

		mEffectsCh = mHge->Effect_PlayEx(mEffects.at(temp.id));
	}
	//music
	else if(temp.type == S_MUSIC)
	{
		if (temp.id == -1337)
		{
			if (mSecret == 0)
			{
				mSecret = mHge->Effect_Load("Sound/AllBases.mp3");
				if (mSecret == 0)
					return false;
			}
			silence(temp.type);
			mMusicCh = mHge->Effect_PlayEx(mSecret);
			return true;
		}

		if((size_t)temp.id < (size_t)0 || (size_t)temp.id > (size_t)mMusic.size())
			return false;

		silence(temp.type);
		mMusicCh = mHge->Effect_PlayEx(mMusic.at(temp.id));
	}
	return true;
}

bool SoundManager::startSound(std::string desc, SoundType type)
{
	bool cont = true;
	int i = 0;

	std::vector<Sound> tList;
	if(type == S_MUSIC)
		tList = mMusicList;
	else if(type == S_EFFECT)
		tList = mEffectList;
	while((size_t)i < (size_t)tList.size())
	{
		if(desc == tList.at(i).desc)
		{
			if(type == S_MUSIC)
			{
				silence(type);
				mMusicCh = mHge->Effect_Play(mMusic.at(i));
			}
			else if(type == S_EFFECT)
				mEffectsCh = mHge->Effect_Play(mEffects.at(i));

			return true;
		}
		++i;
	}
	return false;
}


void SoundManager::silence(SoundType type)
{
	//Effect
	if(type == S_EFFECT)
	{
		mHge->Channel_Stop(mEffectsCh);
	}	
	//Music
	else if(type == S_MUSIC)
	{
		mHge->Channel_Stop(mMusicCh);
	}

}

void SoundManager::silenceAll()
{
	mHge->Channel_StopAll();
}

bool SoundManager::checkMusicSilence()
{
	if(mHge->Channel_IsPlaying(mMusicCh))
		return true;
	return false;
}

void SoundManager::setVolume(int v)
{
	mHge->System_SetState(HGE_FXVOLUME, v);
}