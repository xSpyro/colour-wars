#include "MusicList.h"

MusicList::MusicList()
{
	mVol = 100;
	mAt = -1;
	mForward = false;
	mPlaying = true;
}
bool MusicList::init(SoundManager *temp)
{
	mSM = temp;
	return true;
}

void MusicList::update()
{
	if(!mSM->checkMusicSilence() && mPlaying)
	{
		if(mForward)
			next();
		else
			prev();
	}
}

bool MusicList::addSong(std::string name)
{
	//ifall man l�gger till att spelaren ska kunna l�gga till egen musik
	//s� anv�nder man den h�r funktionen
	std::string temp = mSM->addSound(name, S_MUSIC, "");
	if(temp == "ERROR")
		return false;

	mList.push_back(temp);

	return true;
}

void MusicList::next()
{
	++mAt;
	if((size_t)mAt >= (size_t)mList.size())
		mAt = 0;
	if(mList.size() > 0)
		mSM->startSound(Sound(S_MUSIC, mAt));
}

void MusicList::prev()
{
	--mAt;
	if(mAt < 0)
		mAt = mList.size()-1;
	mSM->startSound(Sound(S_MUSIC, mAt));
}

bool MusicList::loadDefault()
{


	std::string temp = mSM->addSound("Sound/WaterFlame.mp3", S_MUSIC, "");
	if(temp != "ERROR")
		mList.push_back(temp);

	temp = mSM->addSound("Sound/Juggernaut.mp3", S_MUSIC, "");
	if(temp != "ERROR")
		mList.push_back(temp);

	temp = mSM->addSound("Sound/Advance.mp3", S_MUSIC, "");
	if(temp != "ERROR")
		mList.push_back(temp);


	temp = mSM->addSound("Sound/Destination.mp3", S_MUSIC, "");
	if(temp != "ERROR")
		mList.push_back(temp);


	return true;
}

void MusicList::switchDir()
{
	mForward = !mForward;
}

void MusicList::setVolume(int v)
{
	mSM->setVolume(v);
}

void MusicList::stop()
{
	if(mPlaying)
		mSM->silence(S_MUSIC);

	mPlaying = !mPlaying;
}

void MusicList::increaseVol()
{
	if(mVol < 100)
	{
		mVol += 10;
		mSM->setVolume(mVol);
	}
}

void MusicList::decreaseVol()
{
	if(mVol > 0)
	{
		mVol -= 10;
		mSM->setVolume(mVol);
	}
}

void MusicList::playSecret()
{
	mSM->startSound(Sound(S_MUSIC, -1337));
}