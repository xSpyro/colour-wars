#ifndef MUSICLIST_H
#define MUSICLIST_H
#include "SoundManager.h"

class MusicList
{
public:
	MusicList();
	bool init(SoundManager *temp);
	void update();
	bool addSong(std::string name);
	void next();
	void prev();
	bool loadDefault();
	void switchDir();
	void setVolume(int v);
	void stop();
	void increaseVol();
	void decreaseVol();

	void playSecret();

private:
	int mAt,
		mVol;
	bool mForward,
		 mPlaying;

	std::vector<std::string> mList;
	SoundManager *mSM;

};
#endif