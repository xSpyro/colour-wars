
#include "Program.h"
#include "SceneTestPrg.h"
//#include <hge.h>

Program * PRG = 0;

bool frameFunc()
{
	return PRG->hgeUpdate();
}

bool renderFunc()
{
	return PRG->hgeRender();
}

bool restoreFunc()
{
	return PRG->hgeRestore();
}

int main()//WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	HGE * hge = hgeCreate(HGE_VERSION);

	PRG = new SceneTest();

	hge->System_SetState(HGE_FRAMEFUNC, frameFunc);
	hge->System_SetState(HGE_RENDERFUNC, renderFunc);
	hge->System_SetState(HGE_GFXRESTOREFUNC, restoreFunc);

	PRG->initHge(hge);

	if (hge->System_Initiate())
	{
		PRG->initPrg();
		hge->System_Start();
	}
	else
	{
		exit(666);
	}

	PRG->terminate();

	hge->System_Shutdown();
	hge->Release();

}
