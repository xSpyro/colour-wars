#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H
#include <string>
#include <vector>
#include <map>

class Scene;

class SceneMgr
{
public:
	SceneMgr();
	virtual ~SceneMgr();
	void init();
	bool update(float dt);
	void render();
	bool restore();

	bool changeScene(std::string name, bool resumeIfPaused = 1);
	bool pauseAndChange(std::string name, bool resumeIfPaused = 1);

	void exit(); // Will not set a current scene
	void exitAndResume();
	void pause(); // Will not set a current scene

	bool havePaused();

	bool addScene(std::string name, Scene * scene, bool overwrite = 0); // Returns false if there were a scene with that name
private:
	typedef std::map<std::string, Scene *> SceneMap;
	typedef std::vector<Scene *> SceneVector;

	Scene * mCurrent;
	SceneVector mPaused;
	SceneMap mScenes;

	void change(Scene * scene, bool resumeIfPuased);
};

#endif