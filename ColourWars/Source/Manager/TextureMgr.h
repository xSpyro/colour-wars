#ifndef TEXTURE_MGR_H
#define TEXTURE_MGR_H
#include <map>
#include <string>

#include <hge.h>
#include <hgefont.h>

class TextureMgr
{
public:
	TextureMgr(HGE * hge);

	HRESULT getTexture(std::string file);
private:
	typedef std::map<std::string, HTEXTURE> TexMap;

	TexMap mTex;

	HGE * mHge;
};

#endif