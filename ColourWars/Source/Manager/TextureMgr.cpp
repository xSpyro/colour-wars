#include "TextureMgr.h"

TextureMgr::TextureMgr(HGE * hge) : mHge(hge)
{}

HRESULT TextureMgr::getTexture(std::string file)
{
	HRESULT rtn = mTex[file];
	if (rtn == 0)
	{
		rtn = mHge->Texture_Load(file.c_str());

		mTex[file] = rtn;
	}
	return rtn;
}