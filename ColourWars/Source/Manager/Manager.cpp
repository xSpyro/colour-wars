#include "Manager.h"

Manager::Manager(HGE *hge) : mHge(hge), mFont(new FontMgr(mHge)), mTex(new TextureMgr(mHge)), mScene(new SceneMgr()), mSound(new SoundManager(hge)), mMList(new MusicList())
{}

HGE * Manager::getHge()
{
	return mHge;
}

TextureMgr * Manager::getTexMgr()
{
	return mTex;
}

FontMgr * Manager::getFontMgr()
{
	return mFont;
}

SceneMgr * Manager::getSceneMgr()
{
	return mScene;
}

SoundManager * Manager::getSoundMgr()
{
	return mSound;
}

MusicList * Manager::getMusicList()
{
	return mMList;
}

void Manager::setTeamColour(int id, hgeColor col)
{
	mTeamCol[id] = col;
}

hgeColor Manager::getTeamColour(int id)
{
	hgeColor rtn = mTeamCol[id];
	return rtn;
}