#ifndef FONT_MGR_H
#define FONT_MGR_H
#include <map>
#include <string>

#include <hge.h>
#include <hgefont.h>

class FontMgr
{
public:
	FontMgr(HGE * hge);

	hgeFont * getFont(std::string file, std::string use = "");
private:
	typedef std::map<std::string, hgeFont *> FontMap;

	FontMap mFont;

	HGE * mHge;
};

#endif