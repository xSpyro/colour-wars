#include "FontMgr.h"

FontMgr::FontMgr(HGE * hge) : mHge(hge)
{}

hgeFont * FontMgr::getFont(std::string file, std::string use)
{
	std::string temp = "\"" + file + "\"" + use;
	hgeFont * rtn = mFont[temp];
	if (rtn == 0)
	{
		rtn = new hgeFont(file.c_str());

		mFont[temp] = rtn;
	}
	return rtn;
}