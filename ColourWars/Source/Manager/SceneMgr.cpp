#include "SceneMgr.h"
#include "../Scene.h"
#include <algorithm>

SceneMgr::SceneMgr() : mCurrent(0)
{}

SceneMgr::~SceneMgr()
{
	for (SceneMap::iterator it = mScenes.begin(); it != mScenes.end(); it++)
	{
		delete it->second;
	}
	mScenes.clear();
}

void SceneMgr::init()
{
	for (SceneMap::iterator it = mScenes.begin(); it != mScenes.end(); it++)
	{
		delete it->second;
	}
	mScenes.clear();
	mPaused.clear();
}

bool SceneMgr::update(float dt)
{
	if (mCurrent != 0)
		return mCurrent->update(dt);

	return false;
}

void SceneMgr::render()
{
	if (mCurrent != 0)
		return mCurrent->render();
}

bool SceneMgr::restore()
{
	if (mCurrent != 0)
		return mCurrent->restore();
	return false;
}

bool SceneMgr::changeScene(std::string name, bool resumeIfPuased)
{
	Scene * temp = mScenes[name];
	if (temp == 0)
		return false;

	if (mCurrent != 0)
		mCurrent->exitScene();

	change(temp, resumeIfPuased);
	return true;
}

bool SceneMgr::pauseAndChange(std::string name, bool resumeIfPuased)
{
	Scene * temp = mScenes[name];
	if (temp == 0)
		return false;

	if (mCurrent != 0)
	{
		mCurrent->onPause();
		mPaused.push_back(mCurrent);
	}

	change(temp, resumeIfPuased);
	return true;
}

void SceneMgr::exit()
{
	if (mCurrent != 0)
		mCurrent->exitScene();
	mCurrent = 0;
}

void SceneMgr::exitAndResume()
{
	exit();

	if (mPaused.size() > 0)
	{
		mCurrent = mPaused.back();
		mPaused.pop_back();
	}
}

void SceneMgr::pause()
{
	if (mCurrent != 0)
	{
		mCurrent->onPause();
		mPaused.push_back(mCurrent);
	}
	
	mCurrent = 0;
}

bool SceneMgr::havePaused()
{
	return (!mPaused.empty());
}

bool SceneMgr::addScene(std::string name, Scene * scene, bool overwrite)
{
	if (mScenes[name] != 0)
	{
		if (overwrite)
		{
			delete mScenes[name];
			mScenes[name] = scene;
			return true;
		}
		return false;
	}
	mScenes[name] = scene;
	return true;
}

void SceneMgr::change(Scene * scene, bool resumeIfPuased)
{
	int n;
	for (n = mPaused.size() - 1; n >= 0 && mPaused[n] != scene; --n)
	{}

	if (n >= 0)
	{
		mPaused.erase(mPaused.begin() + n);
		if (resumeIfPuased)
		{
			scene->onResume();
			mCurrent = scene;
			return;
		}
		scene->exitScene();
	}
	scene->enterScene();
	mCurrent = scene;
}