#ifndef SCENE_TEST_H
#define SCENE_TEST_H
#include "Program.h"
#include "Scene.h"
#include "Manager/Manager.h"
#include "Sound/MusicList.h"

class SceneTest : public Program
{
public:
	SceneTest();
	virtual ~SceneTest();
	bool initHge(HGE * hge);
	bool initPrg();

	bool update(float dt);
	void render();
	bool restore();
	
	void terminate();
private:
	MusicList *mMusicList;
};

#endif