#ifndef TextButton_H
#define TextButton_H

#include "Button.h"

#include "../Manager/Manager.h"

//Overrides functions from HGE, therefore big letter at begining
class TextButton : public Button
{
public:
	//Det som fanns med n�r man kollade i exemplet
	TextButton(int tId, float tX, float tY, float tDelay, char *tTitle, Manager *tMgr);
	//det som beh�vs: ID, position i X-led, position i Y-led, vad som ska st�, ifall den �r aktiv
	TextButton(int tId, float tX, float tY, char* tTitle, bool tEnabled, Manager *tMgr);

	virtual void Render();
	virtual void Update(float dt);

	virtual void Enter();
	virtual void Leave();
	virtual bool IsDone();
	virtual void Focus(bool bFocused);
	virtual void MouseOver(bool bOver);

	virtual bool MouseLButton(bool bDown);
	virtual bool KeyClick(int key, int chr);

private:
	Manager	*mMgr;
	hgeFont *fnt;
	char *title;
	HEFFECT snd;
	float delay;

	hgeColor scolor, dcolor, scolor2, dcolor2, sshadow, dshadow;
	hgeColor color, shadow;
	float soffset, doffset, offset;
	float timer, timer2;
};
#endif