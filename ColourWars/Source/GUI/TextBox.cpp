#include "Textbox.h"

TextBox::TextBox(int tLimiter, int tID, hgeFont *tFnt, float tX, float tY, float tW, HTEXTURE tTex)
{
	id = tID;
	mFnt = tFnt;
	mXPos = tX;
	mYPos = tY;
	mWidth = tW;
	mActive = false;
	mText = "";
	mTex = tTex;
	mLimiter = tLimiter;

	bStatic = false;
	bVisible = true;
	bEnabled = true;
	color.SetHWColor(0xFFFFE060);

	rect.Set(tX, tY, tX+tW, tY+mFnt->GetHeight());

	mSprite = new hgeSprite(mTex, 0, 0, tW, mFnt->GetHeight());
	
}

void TextBox::init()
{
	mActive = false;
	mText = "";
}

bool TextBox::update(float dt)
{
	return true;
}

void TextBox::Render()
{
	mSprite->Render(rect.x1, rect.y1);
	mFnt->SetColor(color.GetHWColor());
	mFnt->Render(rect.x1, rect.y1, HGETEXT_LEFT, mText.c_str());
}

std::string TextBox::getText()
{
	return mText;
}

void TextBox::setText(std::string text)
{
	mText = text;
}

void TextBox::erase()
{
	setText("");
}

void TextBox::MouseOver(bool bOver)
{
	mOver = bOver;
}

void TextBox::Focus(bool bFocused)
{
	mActive = bFocused;

}

bool TextBox::MouseLButton(bool bDown)
{
	if(mOver && bDown && !mActive)
	{
		gui->SetFocus(id);
		return true;
	}
	return false;
}

bool TextBox::KeyClick(int key, int chr)
{

	if(mActive)
	{
		switch(key)
		{
			case HGEK_BACKSPACE:
				if(mText.size() > 0)
					mText.erase(--mText.end());
				break;

			case HGEK_TAB:
				break;
			case HGEK_ENTER:
				//kommer troligen g�ra n�gonting... kanske g�rs utanf�r den h�r funktionen eller s�
				break;
			default:
				if(isalnum(chr) || ispunct(chr) || isspace(chr))
				{
					if(mLimiter > 0)
					{
						if(mFnt->GetStringWidth(mText.c_str()) < mWidth-10 && mText.size() < mLimiter)
							mText = mText + (char)chr;
					}
					else
					{
						if(mFnt->GetStringWidth(mText.c_str()) < mWidth-10)
							mText = mText + (char)chr;
					}
				}
				break;
		}
	}

	return false;
}