#include "TextButton.h"

TextButton::TextButton(int tId, float tX, float tY, float tDelay, char *tTitle, Manager *tMgr):Button(tId, true)
{
	mMgr = tMgr;
	float w;

	fnt = mMgr->getFontMgr()->getFont("Font/Text20.fnt");
	delay = tDelay;
	title = tTitle;

	color.SetHWColor(0xFFFFE060);
	shadow.SetHWColor(0x30000000);
	offset=0.0f;
	timer=-1.0f;
	timer2=-1.0f;


	w=fnt->GetStringWidth(title);
	rect.Set(tX-w/2, tY, tX+w/2, tY+fnt->GetHeight());
}

TextButton::TextButton(int tId, float tX, float tY, char* tTitle, bool tEnabled, Manager *tMgr):Button(tId, tEnabled)
{
	mMgr = tMgr;
	float w;

	fnt = mMgr->getFontMgr()->getFont("Font/Text20.fnt");
	delay = 0.3f;
	title = tTitle;

	color.SetHWColor(0xFFFFE060);
	shadow.SetHWColor(0x30000000);
	offset = 0.0f;
	timer = -1.0f;
	timer2 = -1.0f;
	


	w = fnt->GetStringWidth(title);
	rect.Set(tX-w/2, tY, tX+w/2, tY+fnt->GetHeight());
}

void TextButton::Render()
{
	fnt->SetColor(shadow.GetHWColor());
	fnt->Render(rect.x1+offset+3, rect.y1+3, HGETEXT_LEFT, title);
	fnt->SetColor(color.GetHWColor());
	fnt->Render(rect.x1-offset, rect.y1-offset, HGETEXT_LEFT, title);
}

void TextButton::Update(float dt)
{
	if(timer2 != -1.0f)
	{
		timer2+=dt;
		if(timer2 >= delay+0.1f)
		{
			color=scolor2+dcolor2;
			shadow=sshadow+dshadow;
			offset=0.0f;
			timer2=-1.0f;
		}
		else
		{
			if(timer2 < delay) { color=scolor2; shadow=sshadow; }
			else { color=scolor2+dcolor2*(timer2-delay)*10; shadow=sshadow+dshadow*(timer2-delay)*10; }
		}
	}
	else if(timer != -1.0f)
	{
		timer+=dt;
		if(timer >= 0.2f)
		{
			color=scolor+dcolor;
			offset=soffset+doffset;
			timer=-1.0f;
		}
		else
		{
			color=scolor+dcolor*timer*5;
			offset=soffset+doffset*timer*5;
		}
	}
}

void TextButton::Enter()
{
	hgeColor tcolor2;

	scolor2.SetHWColor(0x00FFE060);
	tcolor2.SetHWColor(0xFFFFE060);
	dcolor2=tcolor2-scolor2;

	sshadow.SetHWColor(0x00000000);
	tcolor2.SetHWColor(0x30000000);
	dshadow=tcolor2-sshadow;

	timer2=0.0f;
}

void TextButton::Leave()
{
	hgeColor tcolor2;

	scolor2.SetHWColor(0xFFFFE060);
	tcolor2.SetHWColor(0x00FFE060);
	dcolor2=tcolor2-scolor2;

	sshadow.SetHWColor(0x30000000);
	tcolor2.SetHWColor(0x00000000);
	dshadow=tcolor2-sshadow;

	timer2=0.0f;
}

bool TextButton::IsDone()
{
	if(timer2 == -1.0f)
		return true;
	else return false;
}

void TextButton::Focus(bool bFocused)
{
	hgeColor tcolor;

	if(bFocused)
	{
		hge->Effect_Play(snd);
		scolor.SetHWColor(0xFFFFE060);
		tcolor.SetHWColor(0xFFFFFFFF);
		soffset=0;
		doffset=4;
	}
	else
	{
		scolor.SetHWColor(0xFFFFFFFF);
		tcolor.SetHWColor(0xFFFFE060);
		soffset=4;
		doffset=-4;
	}

	dcolor=tcolor-scolor;
	timer=0.0f;
}

void TextButton::MouseOver(bool bOver)
{
	if(bOver)
		gui->SetFocus(id);
}

bool TextButton::MouseLButton(bool bDown)
{
	if(!bDown)
	{
		offset=4;
		return true;
	}
	else 
	{
		hge->Effect_Play(snd);
		offset=0;
		return false;
	}
}
bool TextButton::KeyClick(int key, int chr)
{
	if(key==HGEK_ENTER || key==HGEK_SPACE)
	{
		MouseLButton(true);
		return MouseLButton(false);
	}

	return false;
}