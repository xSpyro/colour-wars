#include "Slider.h"


Slider::Slider()
{

}
Slider::~Slider()
{

}

void Slider::init(hgeGUI * gui, float min, float max, int id, float x, float y, float w, float h,
		HTEXTURE slider, float tx, float ty, float sw, float sh, hgeSprite * bckgr, bool vertical)
{
	mMin = min;
	mMax = max;
	mGUI = gui;

	mSlider = new hgeGUISlider(id, x, y, w, h, slider, 0, 0, sw, sh, vertical);

	mBackground = bckgr;

	mW = w;
	mH = h;

	mX = x;
	mY = y;

	mSW = sw;
	mSH = sh;

	mSlider->SetMode(min, max, HGESLIDER_SLIDER);
	mSlider->SetValue(max);
	mGUI->AddCtrl(mSlider);
}
void Slider::destroy()
{
	delete mSlider;
	mSlider = NULL;
}
float Slider::getValue()
{
	return mMax - mSlider->GetValue();
}
void Slider::setValue(float val)
{
	mSlider->SetValue(mMax - val);
}
void Slider::setMode(float min, float max, int mode)
{
	mMin = min;
	mMax = max;

	mSlider->SetMode(min, max, mode);
}
void Slider::render()
{
	mBackground->RenderStretch(mX, mY-mSH/2, mX+mW, mY+mH+mSH/2);
}



