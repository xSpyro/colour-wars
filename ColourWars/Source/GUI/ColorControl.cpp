#include "ColorControl.h"

ColorControl::ColorControl()
{
	mBackground = NULL;
	mColor      = NULL;
}
ColorControl::~ColorControl()
{

}
/*void ColorControl::init(hgeGUI * gui, float x, float y, float w, float h,
		hgeSprite* ccBckr, hgeSprite* slBckgr, HTEXTURE slide,
		float sw, float sh)
{
	mBackground = ccBckr;

	float slideH = h - (h/4);
	float slideW = w / 8;
	float stepX  = w / 7;
	float sliderH = 10;

	float slideY = y + (h/4)-(slideH/12);

	mX = x;
	mY = y;

	mW = w;
	mH = h;

	int id = 90;

	mRed.init(gui, 0, 255, id, x+stepX, slideY, sw, sh, slide, 0, 0, sw, sliderH, slBckgr, true);

	id++;
	mGreen.init(gui, 0, 255, id, x+stepX*3, slideY, sw, sh, slide, 0, 0, sw, sliderH, slBckgr, true);

	id++;
	mBlue.init(gui, 0, 255, id, x+stepX*5, slideY, sw, sh, slide, 0, 0, sw, sliderH, slBckgr, true);

}*/
void ColorControl::init(hgeGUI * gui, float x, float y, float w, float h,
	hgeSprite* ccBckr, hgeSprite* slBckgr, HTEXTURE r, HTEXTURE g, HTEXTURE b)
{
	//mGUI = gui;

	mBackground = ccBckr;
	
	mX = x;
	mY = y;
	mW = w;
	mH = h;

	float sliderH = h - (h/2);
	float sliderW = w / 8;
	float stepX   = w / 7;
	float sliderY = y + h/4-(sliderH/12);

	int id = 90;

	mRed.init(gui, 0, 255, id, x+stepX, sliderY, sliderW, sliderH, r, 0, 0, sliderW, sliderH/12, slBckgr, true);
	id++;
	mGreen.init(gui, 0, 255, id, x+stepX*3, sliderY, sliderW, sliderH, g, 0, 0, sliderW, sliderH/12, slBckgr, true);
	id++;
	mBlue.init(gui, 0, 255, id, x+stepX*5, sliderY, sliderW, sliderH, b, 0, 0, sliderW, sliderH/12, slBckgr, true);
}
void ColorControl::render()
{
	mBackground->Render(mX, mY);
	mColor->SetColor(getColor().GetHWColor());
	mColor->Render(mX+mW/2-10, mY+mH-30);
	//mBackground->RenderStretch(mX, mY, mX+mW, mY+mH);

	mRed.render();
	mGreen.render();
	mBlue.render();

}
hgeColor ColorControl::getColor()
{
	hgeColor tmp;

	tmp.r = mRed.getValue();
	tmp.g = mGreen.getValue();
	tmp.b = mBlue.getValue();

	tmp *= (float)1/255;

	tmp.a = 1.0f;

	return tmp;
}
void ColorControl::setValue(float r, float g, float b)
{
	mRed.setValue(r);
	mGreen.setValue(g);
	mBlue.setValue(b);
}
void ColorControl::setRValue(float r)
{
	//if( (int)r != (int)(mRed.getValue()) )
	//{
	//	hgeColor t = getColor();
	//	t *= 255;

	//	int rest = abs(255-r); // delta color between sliders old position and new position.
	//	float pG = (255-t.g)/255, pB = (255-t.b)/255;

	//	int G = rest*pG, B = rest*pB;

	//	mGreen.setValue(G);
	//	mBlue.setValue(B);

		mRed.setValue(r);
	//}
}
void ColorControl::setGValue(float g)
{
	//if(g != (int)(mGreen.getValue()*255) )
	//{
	//	hgeColor t = getColor();
	//	t *= 255;

	//	int rest = abs(255-g); // delta color between sliders old position and new position.
	//	float pR = (255-t.r)/255, pB = (255-t.b)/255;

	//	int R = rest*pR, B = rest*pB;

	//	mRed.setValue(R);
	//	mBlue.setValue(B);

		mGreen.setValue(g);
	//}
}
void ColorControl::setBValue(float b)
{
	//if(b != (int)(mBlue.getValue()*255) )
	//{
	//	hgeColor t = getColor();
	//	t *= 255;

	//	int rest = abs(255-b); // delta color between sliders old position and new position.
	//	float pR = (255-t.r)/255, pG = (255-t.g)/255;

	//	int R = rest*pR, G = rest*pG;

	//	mRed.setValue(R);
	//	mGreen.setValue(G);

		mBlue.setValue(b);
	//}
}


