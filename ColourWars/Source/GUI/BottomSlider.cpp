#include "BottomSlider.h"

BottomSlider::BottomSlider()
{
}

BottomSlider::BottomSlider(float x1, float y1, float x2, float y2, hgeGUI *tGui, int tW, int tH, Manager *tMgr)
{
	mMgr = tMgr;
	mHeight = tH;
	mWidth	= tW;
	mGui = tGui;
	init(x1, y1, x2, y2);
	mPosTo = 0.5f;
	mPosNow = 0.5f;
	mSpeed = 0.0f;
}

void BottomSlider::init(float x1, float y1, float x2, float y2)
{
	mRect.Set(x1, y1, x2, y2);

	HTEXTURE temp;
	temp = mMgr->getHge()->Texture_Load("Image/B.png");
	mBlue = new hgeSprite(temp, 0, 0, 1, 1);
	temp = mMgr->getHge()->Texture_Load("Image/R.png");
	mRed = new hgeSprite(temp, 0, 0, 1, 1);
}
bool BottomSlider::update(float dt, float tA1, float tA2)
{
	//ifall b�da inte har n�n bas har spelet inte b�rjat �n
	if (tA1 == 0 && tA2 == 0)
		return true;

	//den har vart den ska, den har vart den �r, den vet vilken hastighet den ska r�ra sig i

	mSpeed = 0;

	mPosTo = (tA1/(tA1 + tA2)); 

	mSpeed = (mPosTo - mPosNow) * (dt * 0.5f);

	mPosNow += mSpeed;
	return true;	
}

void BottomSlider::render()
{
	mBlue->RenderStretch(mRect.x1, mRect.y1, mRect.x1 + ((mWidth -150)*mPosNow), mRect.y2);
	mRed->RenderStretch(mRect.x1 + ((mWidth -150)*mPosNow), mRect.y1, mRect.x1 + (mWidth - 150), mRect.y2);
}
