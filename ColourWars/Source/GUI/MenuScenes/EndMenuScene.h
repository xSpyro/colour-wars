#ifndef EndMenuScene_H
#define EndMenuScene_H

#include <hge.h>
#include <hgefont.h>
#include <hgegui.h>
#include <hgeguictrls.h>
#include <string>

#include "../../Scene.h"
#include "../Textbox.h"
#include "../TextButton.h"
#include "../ImageButton.h"
#include "../../Manager/Manager.h"

class EndMenuScene : public Scene
{
public:
	EndMenuScene();
	EndMenuScene(Manager *tMgr);
	virtual void init(Manager *tMgr);
	virtual bool update(float dt);
	virtual void render();
	virtual void enterScene();
	virtual void exitScene();

private:
	hgeGUI		*mGui;
	hgeGUIText	*mText;

	int mID,
		mLastID;


};
#endif