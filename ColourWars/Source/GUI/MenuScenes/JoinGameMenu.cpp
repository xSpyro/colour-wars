#include "JoinGameMenu.h"
#include "../../Logic/Game.h"



//ska ha textbox f�r ip.nr
//ska ha knapp f�r connect
//ska ha knapp f�r att g� bak�t ett steg

JoinGameMenu::JoinGameMenu()
{
}

JoinGameMenu::JoinGameMenu(Manager *tMgr) : Scene(tMgr)
{
	init(tMgr);
}

void JoinGameMenu::init(Manager *tMgr)
{
	mMgr = tMgr;
	HGE * hge = tMgr->getHge();
	mGui = new hgeGUI();

	HTEXTURE tTex = hge->Texture_Load("cursor.png");
	hgeSprite *tMouse = new hgeSprite(tTex, 0, 0, 32, 32);

	float ScWidth = (float)hge->System_GetState(HGE_SCREENWIDTH);
	float ScHeight = (float)hge->System_GetState(HGE_SCREENHEIGHT);


	hgeFont *tFont = mMgr->getFontMgr()->getFont("Font/Text35.fnt", "text");
	mText = new hgeGUIText(4, ScWidth / 2 -160, ScHeight / 3 -100, 30.0f, 36.0f, tFont);
	mText->SetText("IP-Adress: ");
	mGui->AddCtrl(mText);

	//The textbox
	HTEXTURE temp;
	temp = hge->Texture_Load("Image/TextBox.jpg");
	mTextBox = new TextBox(15, 3, tFont, ScWidth/2 - 125, ScHeight / 3 - 100, 250, temp);
	mGui->AddCtrl(mTextBox);

	HTEXTURE tempAr[2];
	tempAr[0] = hge->Texture_Load("Image/Buttons/Connect1.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/Connect2.png");
	mGui->AddCtrl(new ImageButton(1, ScWidth/2 - 150, ScHeight / 3, 300, 60, tempAr));

	tempAr[0] = hge->Texture_Load("Image/Buttons/Back1.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/Back2.png");
	mGui->AddCtrl(new ImageButton(2, ScWidth/2 - 150, ScHeight / 3 + 120, 300, 60, tempAr));

	mGui->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
	//mGui->SetCursor(tMouse);
	mGui->SetFocus(3);
}

bool JoinGameMenu::update(float dt)
{
	mID = mGui->Update(dt);
	if(mID == -1)
	{
		switch(mLastID)
		{
			case 1:
				//Ta text fr�n textboxen och connecta till den
				//byt �ven scen till gameScene
				{
					Game * g = new Game();
					g->init(mMgr);
					g->connectTo(8888, mTextBox->getText().c_str());
					mMgr->getSceneMgr()->addScene("Game", g, true);
				}
				mMgr->getSceneMgr()->changeScene("Game", true);
				break;
			case 2:
				//G� bak�t till MainMenu
				mMgr->getSceneMgr()->changeScene("MainMenu", true);
				break;
		}
	}
	else if(mID)
	{
		mLastID = mID;
		mGui->Leave();
	}
	return true;
}

void JoinGameMenu::render()
{
	HGE * hge = mMgr->getHge();
	hge->Gfx_BeginScene();

	hge->Gfx_Clear(0x00000000);

	mGui->Render();

	hge->Gfx_EndScene();
}

void JoinGameMenu::enterScene()
{
	mGui->SetFocus(3);
	mGui->Enter();
}

void JoinGameMenu::exitScene()
{
	mGui->Leave();
}