#ifndef HELPSCENE_H
#define HELPSCENE_H

#include <hge.h>
#include <hgefont.h>
#include <hgegui.h>
#include <hgeguictrls.h>
#include <string>

#include "../../Scene.h"
#include "../TextButton.h"
#include "../ImageButton.h"
#include "../../Manager/Manager.h"

class HelpScene : public Scene
{
public:
	HelpScene();
	HelpScene(Manager *tMgr);
	virtual void init(Manager *tMgr);
	virtual bool update(float dt);
	virtual void render();
	virtual void enterScene();
	virtual void exitScene();

private:
	hgeGUI		*mGui;
	int mID,
		mPos;
	hgeSprite	*mSpr;
	HTEXTURE	*mTex;

};
#endif