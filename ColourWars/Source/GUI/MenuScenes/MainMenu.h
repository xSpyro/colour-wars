#ifndef MAINMENU
#define MAINMENU

#include <hge.h>
#include <hgefont.h>
#include <hgegui.h>
#include <hgeguictrls.h>
#include <string>

#include "../../Scene.h"
#include "../TextButton.h"
#include "../ImageButton.h"
#include "../../Manager/Manager.h"

class MainMenu : public Scene
{
public:
	MainMenu();
	MainMenu(Manager *tMgr);
	virtual void init(Manager *tMgr);
	virtual bool update(float dt);
	virtual void render();
	virtual void enterScene();
	virtual void exitScene();

private:
	hgeGUI			*mGui;

	HTEXTURE		mTex;
	hgeSprite		*mMouse;
	hgeFont			*mFont;
	int				mID;
	int				mLastID;

	int mRevision;
	float mRevTime;
};
#endif