#include "HelpScene.h"

HelpScene::HelpScene() : Scene()
{
	mPos = 0;
}

HelpScene::HelpScene(Manager *tMgr) : Scene(tMgr)
{
	mPos = 0;
	init(tMgr);
}

void HelpScene::init(Manager *tMgr)
{
	mMgr = tMgr;
	HGE * hge = tMgr->getHge();
	mGui = new hgeGUI();
	mTex = new HTEXTURE[5];

	float ScWidth = (float)hge->System_GetState(HGE_SCREENWIDTH);
	float ScHeight = (float)hge->System_GetState(HGE_SCREENHEIGHT);


	HTEXTURE temp = hge->Texture_Load("Image/walloftext.png");
	mSpr = new hgeSprite(temp, 0, 0, 900, 410);

	//de bilder som ska anv�ndas laddas in i mTex en efter en i r�tt ordning
	mTex[0] = hge->Texture_Load("Image/Tutorial/Pic1.png");
	mTex[1] = hge->Texture_Load("Image/Tutorial/Pic2.png");
	mTex[2] = hge->Texture_Load("Image/Tutorial/Pic3.png");
	mTex[3] = hge->Texture_Load("Image/Tutorial/Pic4.png");
	mSpr = new hgeSprite(mTex[0], 0, 0, 800, 600);

	//sen ska tre knappar skapas, en "Previous" en "Next" och en "MainMenu"

	HTEXTURE tempAr[2];
	tempAr[0] = hge->Texture_Load("Image/Buttons/MainMenu1-3.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/MainMenu2-3.png");
	mGui->AddCtrl(new ImageButton(1, ScWidth/100, ScHeight / 1.112f, 300, 60, tempAr));

	tempAr[0] = hge->Texture_Load("Image/Buttons/Next1.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/Next2.png");
	mGui->AddCtrl(new ImageButton(2, ScWidth - 160, ScHeight / 1.112f, 160, 60, tempAr));

	tempAr[0] = hge->Texture_Load("Image/Buttons/Back1-2.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/Back2-2.png");
	mGui->AddCtrl(new ImageButton(3, ScWidth/2, ScHeight / 1.112f, 160, 60, tempAr));
}

bool HelpScene::update(float dt)
{
	mID = mGui->Update(dt);
	
	if(mID == 1)
		mMgr->getSceneMgr()->changeScene("MainMenu", true);

	//h�r s� ska det bero p� vad mPos har f�r v�rde s� ska den �ndra bild

	else if(mID == 2 && mPos < 3)
	{
		++mPos;
		mSpr->SetTexture(mTex[mPos]);
	}
	else if(mID == 3 && mPos > 0)
	{
		--mPos;
		mSpr->SetTexture(mTex[mPos]);
	}

	return true;

}

void HelpScene::render()
{
	HGE * hge = mMgr->getHge();
	hge->Gfx_BeginScene();

	hge->Gfx_Clear(0x00000000);

	mGui->Render();
	mSpr->Render(20, 20);

	hge->Gfx_EndScene();
}

void HelpScene::enterScene()
{
	mGui->SetFocus(-1);
	mGui->Enter();
}

void HelpScene::exitScene()
{
	mGui->Leave();
}