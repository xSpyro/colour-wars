#include "MainMenu.h"
#include "../../Logic/Game.h"

MainMenu::MainMenu() : Scene()
{
}

MainMenu::MainMenu(Manager *tMgr) : Scene(tMgr)
{
	init(tMgr);
}

void MainMenu::init(Manager *tMgr)
{
	mRevTime = 0;
	mRevision = 228;

	mMgr = tMgr;
	HGE * hge = tMgr->getHge();
	mGui = new hgeGUI();
	mTex = hge->Texture_Load("cursor.png");
	mMouse = new hgeSprite(mTex, 0, 0, 32, 32);

	mFont = mMgr->getFontMgr()->getFont("Font/Text15.fnt");

	float ScWidth = (float)hge->System_GetState(HGE_SCREENWIDTH);
	float ScHeight = (float)hge->System_GetState(HGE_SCREENHEIGHT);

	HTEXTURE tempAr[2];
	tempAr[0] = hge->Texture_Load("Image/Buttons/JoinGame1.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/JoinGame2.png");

	mGui->AddCtrl(new ImageButton(1, ScWidth/2 - 150, ScHeight/ 3 -120, 300, 60, tempAr));

	tempAr[0] = hge->Texture_Load("Image/Buttons/HostGame1.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/HostGame2.png");
	mGui->AddCtrl(new ImageButton(2, ScWidth/2 - 150, ScHeight/ 3, 300, 60, tempAr));

	tempAr[0] = hge->Texture_Load("Image/Buttons/Help1.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/Help2.png");
	mGui->AddCtrl(new ImageButton(3, ScWidth/2 - 150, ScHeight/ 3 + 120, 300, 60, tempAr));

	tempAr[0] = hge->Texture_Load("Image/Buttons/Exit1.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/Exit2.png");
	mGui->AddCtrl(new ImageButton(4, ScWidth/2 - 150, ScHeight/ 3 + 240, 300, 60, tempAr));


	/*
	//The ImageButton for the gameScene to get to MainMenu
	HTEXTURE tempAr[3];
	tempAr[0] = hge->Texture_Load("Buttons/MainMenu1.png");
	tempAr[1] = hge->Texture_Load("Buttons/MainMenu2.png");
	tempAr[2] = hge->Texture_Load("Buttons/MainMenu3.png");
	mGui->AddCtrl(new ImageButton(4, SCwidth/2 - 150, SCheight/ 6 + 120, 300, 100, tempAr));

	*/

	mGui->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
	//mGui->SetCursor(mMouse);
	mGui->SetFocus(-1);
	mGui->Enter();
}

bool MainMenu::update(float dt)
{
	mID = mGui->Update(dt);
	if(mID == -1)
	{
		switch(mLastID)
		{
			case 1:
				mMgr->getSceneMgr()->changeScene("JoinMenu", true);
				break;
			case 2:
				{
					Game * g = new Game();
					g->init(mMgr);
					g->host(8888);
					mMgr->getSceneMgr()->addScene("Game", g, true);
				}
				mMgr->getSceneMgr()->changeScene("Game", true);
				break;
			case 3:
				mMgr->getSceneMgr()->changeScene("HelpScene", true);
				break;
			case 4:
				return false;
		}
	}
	else if(mID)
	{
		mLastID = mID;
		mGui->Leave();
	}

	mRevTime -= dt;
	while(mRevTime < 0)
	{
		mRevTime += 1;
		mRevision += 1;
	}

	return true;
}

void MainMenu::render()
{
	HGE * hge = mMgr->getHge();
	hge->Gfx_BeginScene();

	hge->Gfx_Clear(0x00000000);

	mGui->Render();
	mFont->printf(0, 0, HGETEXT_LEFT, "Version 0.4.%i", mRevision);

	hge->Gfx_EndScene();
}

void MainMenu::enterScene()
{
	mGui->SetFocus(1);
	mGui->Enter();
}

void MainMenu::exitScene()
{
	mGui->Leave();
}