#include "EndMenuScene.h"

EndMenuScene::EndMenuScene()
{
}

EndMenuScene::EndMenuScene(Manager *tMgr):Scene(tMgr)
{
	init(tMgr);
}

void EndMenuScene::init(Manager *tMgr)
{
	mMgr = tMgr;
	HGE* hge = tMgr->getHge();
	mGui = new hgeGUI();

	float ScWidth = (float)hge->System_GetState(HGE_SCREENWIDTH);
	float ScHeight = (float)hge->System_GetState(HGE_SCREENHEIGHT);

	/*
		Knapp f�r att komma tillbaka till MainMenu

		Text som s�ger ifall man har vunnit eller f�rlorat

		Alternativt:
			Info om hur m�nga baser man byggt, tagit �ver, f�rlorat
			Info om hur m�nga enhter man f�rlorade, d�dade
	*/

	HTEXTURE tempAr[3];
	tempAr[0] = hge->Texture_Load("Image/Buttons/MainMenu1.png");
	tempAr[1] = hge->Texture_Load("Image/Buttons/MainMenu2.png");
	tempAr[2] = hge->Texture_Load("Image/Buttons/MainMenu3.png");
	mGui->AddCtrl(new ImageButton(1, ScWidth/2 - 150, ScHeight / 3, 300, 60, tempAr));

	std::string tText = "Default";
	//tText = get if you won or not
	hgeFont *tFont = mMgr->getFontMgr()->getFont("Font/Text35.fnt", "text");
	mText = new hgeGUIText(2, ScWidth / 2 -160, ScHeight / 3 -100, 30.0f, 36.0f, tFont);
	mText->SetText(tText.c_str());
	mGui->AddCtrl(mText);
}

bool EndMenuScene::update(float dt)
{
	mID = mGui->Update(dt);
	if(mID == -1)
	{
		switch(mLastID)
		{
		case 1:
			//G� bak�t till MainMenu
			mMgr->getSceneMgr()->changeScene("MainMenu", true);
			break;
		}
	}
	else if(mID)
	{
		mLastID = mID;
		mGui->Leave();
	}
	return true;
}

void EndMenuScene::render()
{
	HGE * hge = mMgr->getHge();
	hge->Gfx_BeginScene();

	hge->Gfx_Clear(0x00000000);

	mGui->Render();

	hge->Gfx_EndScene();
}

void EndMenuScene::enterScene()
{
	mGui->SetFocus(-1);
	mGui->Enter();
}

void EndMenuScene::exitScene()
{
	mGui->Leave();
}