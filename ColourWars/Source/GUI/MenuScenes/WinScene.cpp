#include "WinScene.h"

WinScene::WinScene() : Scene()
{
}

WinScene::WinScene(Manager *tMgr) : Scene(tMgr)
{
	init(tMgr);
}

void WinScene::init(Manager *tMgr)
{
	mMgr = tMgr;
	mGui = new hgeGUI();

	float ScWidth = (float)tMgr->getHge()->System_GetState(HGE_SCREENWIDTH);
	float ScHeight = (float)tMgr->getHge()->System_GetState(HGE_SCREENHEIGHT);

	hgeFont *tFont = mMgr->getFontMgr()->getFont("Font/Text35.fnt", "text");
	mText = new hgeGUIText(4, ScWidth / 2, ScHeight / 3, 30.0f, 36.0f, tFont);
	mText->SetText("You Win");
	mGui->AddCtrl(mText);

	HTEXTURE tempAr[2];
	tempAr[0] = tMgr->getHge()->Texture_Load("Image/Buttons/MainMenu1.png");
	tempAr[1] = tMgr->getHge()->Texture_Load("Image/Buttons/MainMenu2.png");
	mGui->AddCtrl(new ImageButton(1, ScWidth/2 - 150, ScHeight/ 3 + 120, 300, 100, tempAr));
}

bool WinScene::update(float dt)
{
	int tID = mGui->Update(dt);

	if(tID == 1)
		mMgr->getSceneMgr()->changeScene("MainMenu", true);
	return true;
}

void WinScene::render()
{
	HGE * hge = mMgr->getHge();
	hge->Gfx_BeginScene();

	hge->Gfx_Clear(0x00000000);

	mGui->Render();

	hge->Gfx_EndScene();
}

void WinScene::enterScene()
{
	mGui->Enter();
}

void WinScene::exitScene()
{
	mGui->Leave();
}
