#ifndef JOINGAMEMENU_H
#define JOINGAMEMENU_H

#include <hge.h>
#include <hgefont.h>
#include <hgegui.h>
#include <hgeguictrls.h>
#include <string>

#include "../../Scene.h"
#include "../Textbox.h"
#include "../TextButton.h"
#include "../ImageButton.h"
#include "../../Manager/Manager.h"

class JoinGameMenu : public Scene
{
public:
	JoinGameMenu();
	JoinGameMenu(Manager *tMgr);

	virtual void init(Manager *tMgr);
	virtual bool update(float dt);
	virtual void render();
	virtual void enterScene();
	virtual void exitScene();

private:
	hgeGUI		*mGui;

	int mID,
		mLastID;
	TextBox		*mTextBox;
	hgeGUIText	*mText;
};
#endif