#ifndef LOSESCENE_H
#define LOSESCENE_H

#include <hge.h>
#include <hgefont.h>
#include <hgegui.h>
#include <hgeguictrls.h>
#include <string>

#include "../../Scene.h"
#include "../TextButton.h"
#include "../ImageButton.h"
#include "../../Manager/Manager.h"

class LoseScene : public Scene
{
public:
	LoseScene();
	LoseScene(Manager *tMgr);
	virtual void init(Manager *tMgr);
	virtual bool update(float dt);
	virtual void render();
	virtual void enterScene();
	virtual void exitScene();


private:
	Manager *mMgr;
	hgeGUI	*mGui;
	hgeGUIText * mText;

};
#endif