#ifndef WINSCENE_H
#define WINSCENE_H

#include <hge.h>
#include <hgefont.h>
#include <hgegui.h>
#include <hgeguictrls.h>
#include <string>

#include "../../Scene.h"
#include "../TextButton.h"
#include "../ImageButton.h"
#include "../../Manager/Manager.h"

class WinScene : public Scene
{
public:
	WinScene();
	WinScene(Manager *tMgr);
	virtual void init(Manager *tMgr);
	virtual bool update(float dt);
	virtual void render();
	virtual void enterScene();
	virtual void exitScene();


private:
	Manager *mMgr;
	hgeGUI	*mGui;
	hgeGUIText * mText;

};
#endif