#ifndef SLIDER_H
#define SLIDER_H

#include <hgeguictrls.h>



class Slider
{


public:

	Slider();
	virtual ~Slider();

	void init(hgeGUI * gui, float min, float max, int id, float x, float y, float w, float h,
		HTEXTURE slider, float tx, float ty, float sw, float sh, hgeSprite * bckgr, bool vertical = false);



	void destroy();

	float getValue();
	void setValue(float val);

	void setMode(float min, float max, int mode);

	void render();


private:

	hgeGUISlider * mSlider;

	hgeGUI * mGUI;

	hgeSprite * mBackground;

	float mX;
	float mY;

	float mW;
	float mH;

	float mMin;
	float mMax;

	float mSW;
	float mSH;



};



#endif



