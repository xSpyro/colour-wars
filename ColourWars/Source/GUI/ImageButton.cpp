#include "ImageButton.h"

ImageButton::ImageButton(int tID, float tX, float tY, float tWidth, float tHeight, HTEXTURE tTex[2]):Button(tID, true)
{
	for(int i=0; i<2; ++i)
		mTexArr[i] = tTex[i];

	mSprite = new hgeSprite(mTexArr[0], 0, 0, tWidth, tHeight);
	rect.Set(tX, tY, tX+tWidth, tY+tHeight);
}

ImageButton::ImageButton(int tID, float tX, float tY, float tWidth, float tHeight, HTEXTURE tTex[2], bool tEnabled):Button(tID, tEnabled)
{
	for(int i=0; i<2; ++i)
		mTexArr[i] = tTex[i];

	mSprite = new hgeSprite(mTexArr[0], 0, 0, tWidth, tHeight);
	rect.Set(tX, tY, tX+tWidth, tY+tHeight);
}

ImageButton::ImageButton(int tID, float tX, float tY, float tWidth, float tHeight, HTEXTURE tTex):Button(tID, true)
{
	for(int i=0; i<2; ++i)
		mTexArr[i] = NULL;
	mTex = tTex;

	mSprite = new hgeSprite(mTex, 0, 0, tWidth, tHeight);
	rect.Set(tX, tY, tX+tWidth, tY+tHeight);
}

ImageButton::ImageButton(int tID, float tX, float tY, float tWidth, float tHeight, HTEXTURE tTex, bool tEnabled):Button(tID, tEnabled)
{
	for(int i=0; i<2; ++i)
		mTexArr[i] = NULL;
	mTex = tTex;

	mSprite = new hgeSprite(mTex, 0, 0, tWidth, tHeight);
	rect.Set(tX, tY, tX+tWidth, tY+tHeight);
}

void ImageButton::Render()
{
	mSprite->Render(rect.x1, rect.y1);
}

void ImageButton::Update(float dt)
{
	/*
		Skulle vara ifall jag f�rs�ker g�r s� att bilderna blandas med varandra n�r de ska bytas

		Men det orkar jag inte just nu, d� det bara �r utsmyckning
	*/
}

void ImageButton::Focus(bool bFocused)
{
	if(bFocused)
	{
		if(mTexArr[1])
			mSprite->SetTexture(mTexArr[1]);
	}
	else if(mTexArr[0])
		mSprite->SetTexture(mTexArr[0]);
}

void ImageButton::MouseOver(bool bOver)
{
	if(bOver)
		gui->SetFocus(id);
	else
		gui->SetFocus(-1);
}

bool ImageButton::MouseLButton(bool bDown)
{
	if(bDown)
	{
		if(mTexArr[0])
			mSprite->SetTexture(mTexArr[0]);
		return true;
	}

	if(mTexArr[0])
		mSprite->SetTexture(mTexArr[0]);
	return false;

}

bool ImageButton::KeyClick(int key, int chr)
{

	return false;
}

void ImageButton::Enter()
{
}

void ImageButton::Leave()
{
}