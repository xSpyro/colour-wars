#ifndef COLORCONTROL_H
#define COLORCONTROL_H

#include "Slider.h"
#include <hgeColor.h>


class ColorControl
{

public:
	
	ColorControl();

	virtual ~ColorControl();

	//void init(hgeGUI * gui, float x, float y, float w, float h,
	//	hgeSprite* ccBckr, hgeSprite* slBckgr, HTEXTURE slide,
	//	float sw, float sh);

	void init(hgeGUI * gui, float x, float y, float w, float h,
		hgeSprite* ccBckr, hgeSprite* slBckgr, HTEXTURE r, HTEXTURE g, HTEXTURE b);

	void setColor(hgeSprite * col)
	{
		mColor = col;
	}

	void render();

	hgeColor getColor();

	void setValue(float r, float g, float b);

	void setRValue(float r);
	void setGValue(float g);
	void setBValue(float b);
	
	//////////////////////////////////////////////////////////////////
	// GET FUNCTIONS GET FUNCTIONS GET FUNCTIONS GET FUNCTIONS
	//////////////////////////////////////////////////////////////////

	float getRValue()
	{
		return mRed.getValue();
	}
	float getGValue()
	{
		return mGreen.getValue();
	}
	float getBValue()
	{
		return mBlue.getValue();
	}

private:

	Slider mRed;
	Slider mGreen;
	Slider mBlue;

	hgeSprite * mBackground;
	hgeSprite * mColor;

	float mX;
	float mY;

	float mW;
	float mH;
};

#endif
