#ifndef BOTTOMSLIDER_H
#define BOTTOMSLIDER_H

#include <hge.h>
#include <hgegui.h>
#include "../Manager/Manager.h"

class BottomSlider
{
public:
	BottomSlider();
	BottomSlider(float x1, float y1, float x2, float y2, hgeGUI *tGui, int screenWidth, int screenHeight, Manager *tMgr);
	void init(float x1, float y1, float x2, float y2);
	bool update(float dt, float tA1, float tA2);
	void render();


private:
	hgeSprite *mBlue, *mRed;
	hgeRect	mRect;
	Manager	*mMgr;
	hgeGUI *mGui;
	int mWidth,
		mHeight;

	float	mPosTo; //�r i b�rjan 0.5f
	float	mPosNow; //�r i b�rjan 0.5f
	float	mSpeed; //�r i b�rjan 0.0f
};
#endif