#ifndef BUTTON_H
#define BUTTON_H

#include <hge.h>
#include <hgegui.h>

class Button : public hgeGUIObject
{
public:
	Button();
	Button(int tID, bool tEnabled);
	virtual void Render() = 0;
	virtual void Update(float dt) = 0;
	virtual void Focus(bool bFocused);
	virtual void MouseOver(bool bOver);
	virtual bool MouseLButton(bool bDown);
	virtual bool KeyClick(int key, int chr);

private:

};
#endif