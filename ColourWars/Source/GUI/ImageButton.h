#ifndef IMAGEBUTTON_H
#define IMAGEBUTTON_H

#include "Button.h"

class ImageButton : public Button
{
public:
	ImageButton(int tID, float tX, float tY, float tWidth, float tHeight, HTEXTURE tTex[2]);
	ImageButton(int tID, float tX, float tY, float tWidth, float tHeight, HTEXTURE tTex[2], bool tEnabled);
	ImageButton(int tID, float tX, float tY, float tWidth, float tHeight, HTEXTURE tTex);
	ImageButton(int tID, float tX, float tY, float tWidth, float tHeight, HTEXTURE tTex, bool tEnabled);

	virtual void Render();
	virtual void Update(float dt);
	virtual void MouseOver(bool bOver);
	virtual void Focus(bool bFocused);
	virtual bool MouseLButton(bool bDown);
	virtual bool KeyClick(int key, int chr);
	virtual void Leave();
	virtual void Enter();


private:
	HTEXTURE	mTexArr[2];
	HTEXTURE	mTex;
	hgeSprite	*mSprite;
};
#endif