#ifndef TEXTBOX_H
#define TEXTBOX_H


#include <hge.h>
#include <hgegui.h>
#include <hgefont.h>
#include <hgecolor.h>
#include <hgesprite.h>

#include <string>

class TextBox : public hgeGUIObject
{
public:
	TextBox(int mLimiter, int tID, hgeFont *tFnt, float tX, float tY, float tW, HTEXTURE tTex);
	void init();
	bool update(float dt);
	void Render();
	std::string getText();
	void setText(std::string text);
	void erase();
	void MouseOver(bool bOver);
	void Focus(bool bFocused);
	bool MouseLButton(bool bDown);
	bool KeyClick(int key, int chr);


private:
	hgeFont		*mFnt;
	std::string mText;
	bool		mActive,
				mOver;
	float		mXPos,
				mYPos,
				mWidth;
	hgeColor	color;
	HTEXTURE	mTex;
	hgeSprite	*mSprite;
	int			mLimiter;


};
#endif