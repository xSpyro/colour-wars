#include "Scene.h"

Scene::Scene()
{
}

Scene::Scene(Manager *tMgr)
{
	init(tMgr);
}

void Scene::init(Manager *tMgr)
{
	mMgr = tMgr;
}

bool Scene::restore()
{
	//ifall den har rendertarget

	return true;
}

void Scene::onPause()
{}

void Scene::onResume()
{}

void Scene::pause()
{
	onPause();
	mPaused = true;
}

void Scene::resume()
{
	onResume();
	mPaused = false;
}

void Scene::enterScene()
{
	//typ konstruktor
}

void Scene::exitScene()
{
	//typ destruktor
}