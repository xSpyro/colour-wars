#include "Server.h"
#include "../Logic/Game.h"
#include "Client.h"
#include "ClientManager.h"
#include <sstream>
#include <float.h>

Server::Server() : mBase(0), mGroup(0), mLimGroup(0), mLimBase(0), mStart(false)
{
	for (int n = 0; n < mMaxClients; ++n)
		mLine.push_back(std::string());
	mNrOfClients=0;	
	ServerSocket = NULL;
	mSendBase = false;
	mResult = GetLastError();
	mTime = 0;
}

int Server::run(int port)
{
	mPort = port;
	if(WSAStartup(MAKEWORD(2,2),&WsaDat)!=0)
	{
		std::cout<<"WSA Initialization failed!\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	ServerSocket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);					
	if(ServerSocket==INVALID_SOCKET)
	{
		std::cout<<"Socket creation failed.\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}
					
	SockAddr.sin_port=htons(mPort);
	SockAddr.sin_family=AF_INET;
	SockAddr.sin_addr.s_addr=htonl(INADDR_ANY);

	if(bind(ServerSocket,(LPSOCKADDR)&SockAddr,sizeof(SockAddr))==SOCKET_ERROR)
	{
		
		std::cout<<"Unable to bind socket!\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	if(listen(ServerSocket, SOMAXCONN) == SOCKET_ERROR )
	{
		std::cout<<"Waiting for incoming connections...\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}
		

	return 0;

}

int Server::update(float dt)
{
	mTime -= dt;
	while (mTime < 0)
	{
		std::string temp = "";
		if (!mSendBase && !mGame->mGroups.empty())
		{
			if (mGroup >= mGame->mGroups.size())
				mGroup = 0;
			temp = mGame->mGroups[mGroup]->msgGroup();
			mGroup++;
		}
		else if (!mGame->mBases.empty())
		{
			if (mBase >= mGame->mBases.size())
				mBase = 0;
			temp = mGame->mBases[mBase]->msgBase();
			mBase++;
		}

		mTime += 0.2;
		mSendBase = !mSendBase;

		if (temp != "")
			sendLine(temp);
	}

	//reading incomming data
	for ( int n = 0; n <mNrOfClients; ++n )
	{
		char szIncoming[1024];
		ZeroMemory(szIncoming, sizeof(szIncoming));


		WSAEventSelect(Socket[n], recv, 0);
					
			
		int inDataLength=recv(Socket[n],
						(char*)szIncoming,
						sizeof(szIncoming)/sizeof(szIncoming[0]),
						0);


		if ( inDataLength != -1 )	//if some message has been recieved
		{
			for ( int i = 0; i < inDataLength; ++i ) 
			{
				if ( szIncoming[i] == '\n' )
				{
					onReadLine(mLine[n], n);
					mLine[n] = "";
				}
				else if (szIncoming[i] != 0)
				{
					mLine[n] += szIncoming[i];
				}
			}					
		}
	}
	int nError=WSAGetLastError();
	if ( nError != WSAEWOULDBLOCK&&nError!=0)
	{
		std::cout<<"Winsock error code: "<<nError<<"\n";
		std::cout<<"Client disconnected!\n";

	
		shutdown(ServerSocket, SD_SEND);
		end();
		return 0;
		
	}

	return 1;
}

void Server::end()
{
	shutdown(ServerSocket, SD_BOTH);
	closesocket(ServerSocket);
	WSACleanup();
}


bool Server::acceptClients()
{
	//accepting clients
	//WSAEventSelect(Socket[mNrOfClients], accept, 0);
	if ( mNrOfClients < mMaxClients )
	{			
		int size = sizeof(sockaddr);
		Socket[mNrOfClients] = accept(ServerSocket, &sockAddrClient, &size);

		if ( Socket[mNrOfClients] == INVALID_SOCKET )
		{
			int nret = WSAGetLastError();
			WSACleanup();
		}
		else
		{
			//std::cout<<"Client Nr: "<<mNrOfClients+1<<" Connected"<<std::endl;
			char *szMessage = "Client Connected\r\n";
			send(Socket[mNrOfClients], szMessage, strlen(szMessage), 0);
			mNrOfClients++;
		}	
		return false;
	}
	return true;
}

bool Server::allConnected()
{
	return  !( mNrOfClients < mMaxClients );
}

void Server::setGame(Game * game)
{
	mGame = game;
}

int Server::getNewBase()
{
	int rtn;
	if (mEmptyBase.empty())
	{
		rtn = mLimBase;
		mLimBase++;
		return rtn;
	}
	rtn = mEmptyBase.back();
	mEmptyBase.pop_back();
	return rtn;
}

int Server::getNewGroup()
{
	int rtn;
	if (mEmptyGroup.empty())
	{
		rtn = mLimGroup;
		mLimGroup++;
		return rtn;
	}
	rtn = mEmptyGroup.back();
	mEmptyGroup.pop_back();
	return rtn;
}

void Server::removeBase(int id)
{
	if (id >= mLimBase - 1)
	{
		mLimBase--;
		return;
	}
	mEmptyBase.push_back(id);
}

void Server::removeGroup(int id)
{
	if (id >= mLimGroup - 1)
	{
		mLimGroup--;
		return;
	}
	mEmptyGroup.push_back(id);
}

void Server::onReadLine(std::string line, int from)
{
	std::stringstream ss;
	int i;
	float x, y;
	float dist, radie, units;
	int id, teamId, groupId;
	bool force;
	hgeColor colour;
	DWORD col;
	int origin, dest;

	ss << line;
	ss >> i;	
	if ( i == MSG_START  )
	{
		mStart = true;
		return;
	}
	ss >>teamId;

	
	switch (i)
	{
	case MSG_MOVE:
		if (teamId == from)
		{
			ss >> id >> col >> dist >> units >> origin >> dest;
			colour.SetHWColor(col);

			Base * t = mGame->findBase(origin);
			if (t->getTeamId() != from)
			{
				std::cout<< "MESSAGE REJECTED, player " << from << " tried to send message, type " << i << ", for another player.\n";
				break;
			}

			move(teamId, colour, dist, units, origin, dest);
		}
		break;
	case MSG_NEWBASE:
		if (teamId == from || !mStart)
		{
			ss >> id >> x >> y >> col >> force >> origin;

			if (((int)mGame->mBaseLimit[from]) > mGame->mNrOfBases[from] || (force && !mStart))
			{
				if (!force && !mStart)
					break;

				colour.SetHWColor(col);

				id = getNewBase();
				newBase(colour, x, y, 10, id, teamId, force, origin);
			}
		}
		break;
	case MSG_SACRIFICE:
		if (teamId == from)
		{
			ss >> id >> units;

			Base * b = mGame->findBase(id);
			if (b != 0)
			{
				if(b->getTeamId() != from)
				{
					std::cout<< "MESSAGE REJECTED, player " << from << " tried to send message, type " << i << ", for another player.\n";
					break;
				}

				b->sacrifice(units);
				sendLine(b->msgBase());
			}
		}
		break;
	case MSG_RMVGROUP:
		if (from == 0) // Server always at 0
		{
			ss >> id;
			removeGroup(id);
			sendLine(line);
		}
		break;
	case MSG_BASECAPTURE:
		if (from == 0)
		{
			int size;
			int remove;
			ss >> id >> units >> size;
			for (int n = 0; n < size; ++n)
			{
				ss >> remove;
				removeGroup(remove);
			}
			sendLine(line);			
		}
		break;
	case MSG_SETDESTCOL:
		if (from == teamId)
		{
			ss >> id >> col;

			colour.SetHWColor(col);
			Base *b = mGame->findBase(id);
			if (b == 0)
				break;

			if (b->getTeamId() != from)
			{
				std::cout<< "MESSAGE REJECTED, player " << from << " tried to send message, type " << i << ", for another player.\n";
				break;
			}

			sendLine(line);
		}
		break;
	case MSG_ARRIVE:
		if (from == 0)
		{
			sendLine(line);
		}
		break;
	default:
		sendLine(line);
		break;
	}
}



void Server::sendLine(const std::string &line)
{
	for (int n = 0; n < mNrOfClients; ++n)
	{
		std::string out = line;
		out += '\n';
		send(Socket[n], out.c_str(), out.length(), 0);
	}
}

bool Server::newBase(hgeColor col, float x, float y, int units, int id, int teamId, bool forceBuilt, int origin)
{
	if ( mGame->inScreen(x, y, 20) == false )
		return false;

	int temp;
	if (!forceBuilt)
	{
		Base *from = mGame->findBase(origin);
		if (from == 0)
			return false;

		std::vector<Base *> bases;
		for(size_t i = 0; i < mGame->mBases.size(); i++)
		{
			if( mGame->mBases[i]->inRadius(x, y) )
				return false;
			else if( !mGame->mBases[i]->inRadius(x, y) && mGame->mBases[i]->inOuterRadius(x, y) )
			{
				// mBases[i]->getRadius()) )
				bases.push_back(mGame->mBases[i]);
			}
		}

		for(size_t i = 0; i < bases.size(); i++)
		{
			if( bases[i]->inOuterRadius(x, y) && bases[i]->getTeamId() == from->getTeamId())
			{
				Base * b = 0;
				Group * g = 0;
				if (!from->buildBase(hgeColor(0, 0, 0, 1), x, y, 10, id, &g, &b))
					return false;

				if (b == 0)
					return false;

				b->setDstColour(col);

				if ( g != 0 )
				{
					if (mGame->findBase(origin) != 0)
						sendLine(b->msgNew(origin));	//origin ska skickas
					else
						sendLine(b->msgNew(-1));

					mGame->mClient->recieve();

					g->setId(getNewGroup());

					sendLine(g->msgMove());
					sendLine(from->msgBase());
				}
				else
					return false;

				break;
			}
		}

	}
	else
	{
		Base *b = new Base(col, x, y, 40, 0, getNewBase(), teamId, mGame->mMgr);
		b->forceBuilt();
		sendLine(b->msgNew(-1));
	}


	return false;
}

bool Server::move(int teamID, hgeColor colour, float toStop, float nrUnits, int origin, int dest)
{
	if (origin == dest)
		return false;
	Base * from = mGame->findBase(origin);
	Base * to = mGame->findBase(dest);

	if (from == 0 || to == 0)
		return false;

	Group * g = from->sendGroupTo(to, nrUnits, getNewGroup(), to->getId());
	
	if (g == 0)
		return false;

	sendLine(g->msgMove());
	sendLine(from->msgBase());
	return true;
}

bool Server::rmvGroup(int groupID, int id)
{
	Group * g = mGame->findGroup(groupID);
	if (g == 0)
		return 0;

	removeGroup(groupID);
	
	std::stringstream ss;
	ss << groupID << " " << id;

	std::string s;
	std::getline(ss, s);
	sendLine(s);
}

bool Server::sacrifice(int baseID, int teamID, float units)
{	
	Base *b = mGame->findBase(baseID);
	if ( b == 0 )
		return 0;

	


	return false;
}