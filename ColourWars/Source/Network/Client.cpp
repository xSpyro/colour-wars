#include "Client.h"
#include <string>
#include "../Logic/Group.h"
#include "../Logic/Base.h"
using namespace std;

Client::Client()
{
	line = " ";
}

int Client::connectTo(int port, const char *host) 
{
	mPort = port;
	if(WSAStartup(MAKEWORD(2,2),&WsaDat)!=0)
	{
		std::cout<<"Winsock error - Winsock initialization failed\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}
	
	// Create our socket
	Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(Socket==INVALID_SOCKET)
	{
		std::cout<<"Winsock error - Socket creation Failed!\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}
	
	// Resolve IP address for hostname
	struct hostent *tHost;
	if((tHost=gethostbyname(host))==NULL)
	{
		std::cout<<"Failed to resolve hostname.\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}
	SockAddr.sin_port=htons(mPort);
	SockAddr.sin_family=AF_INET;
	SockAddr.sin_addr.s_addr=*((unsigned long*)tHost->h_addr);
	
	
	// Attempt to connect to server
	if(connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr))!=0)
	{
		std::cout<<"Failed to establish connection with server\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}
	
	//If iMode!=0, non-blocking mode is enabled.
	u_long iMode=1;
	ioctlsocket(Socket,FIONBIO,&iMode);

	return 1;
}

bool Client::sendLine(const std::string &line)
{
	std::string out = line;
	out += '\n';
	send(Socket, out.c_str(), out.length(), 0);
		
	return true;
}


bool Client::recieve()
{	
	char buffer[1000];
	WSAEventSelect(Socket, recv, 0);

	int inDataLength = recv(Socket, buffer, 1000, 0);
	
	
	if ( inDataLength != -1 )  // if some message has been recieved
	{
		for ( int i = 0; i<inDataLength; ++i ) 
		{
			if ( buffer[i] == '\n' )
			{
				onReadLine(line);
				line = "";
			}
			else if (buffer[i] != 0)
			{
				line += buffer[i];
			}
		}
		

		//if (!line.empty())
		//{
		//	cout<<line<<endl;
		//	//cout<< (int)line.at(0) <<endl;
		//}
		
		int nError=WSAGetLastError();

		if(nError!=WSAEWOULDBLOCK&&nError!=0)
		{
			std::cout<<"Winsock error code: "<<nError<<"\r\n";
			std::cout<<"Server disconnected!\r\n";

			shutdown(Socket,SD_SEND);
			close();
			return false;
		}

		return true;
		
	}
	else
	{
		line = " ";
		memset(buffer,0,1000);
				
		int nError=WSAGetLastError();

		if(nError!=WSAEWOULDBLOCK&&nError!=0)
		{
			std::cout<<"Winsock error code: "<<nError<<"\r\n";
			std::cout<<"Server disconnected!\r\n";

			shutdown(Socket,SD_SEND);

			close();
			return false;
		}
		return true;		
	}
	
	return true;
}

void Client::sendSacrifice(int baseID, int id, float sacrificed)
{
	std::stringstream ss;
	ss << MSG_SACRIFICE << " ";
	ss << id << " ";
	ss << baseID << " ";
	ss << sacrificed << " ";
	std::string temp;
	getline(ss, temp, '\n');
	sendLine(temp);	
}

void Client::sendGroup(Group *g)
{
	std::string temp = g->msgGroup();
	sendLine(temp);
}

void Client::sendMove(Group *g)
{
	std::string temp = g->msgMove();
	sendLine(temp);
}

void Client::sendBase(Base *b)
{
	std::string temp = b->msgBase();
	sendLine(temp);
}

void Client::sendNewBase(Base *b, int origin )
{
	std::string temp = b->msgNew(origin);
	sendLine(temp);
}

void Client::sendBaseCapture(Base * b)
{
	std::string temp = b->msgCaptured();
	sendLine(temp);
}

void Client::sendSetDestCol(Base *b)
{
	std::string temp = b->msgSetDestCol();
	sendLine(temp);
}

void Client::sendSetDestCol(int teamId, int baseId, hgeColor dest )
{
	std::stringstream ss;
	ss << MSG_SETDESTCOL << " ";
	ss << teamId << " ";
	ss << baseId << " ";
	ss << dest.GetHWColor() << " ";

	std::string temp;
	getline(ss, temp, '\n');
	sendLine(temp);	
}



void Client::sendGroup(int groupID, hgeColor colour, float nrOfUnits, float distanceMoved)
{
	std::stringstream ss;
	ss << MSG_GROUP << " ";
	ss << groupID << " ";
	ss << colour.GetHWColor()<< " ";	
	ss << nrOfUnits << " ";
	ss << distanceMoved << " ";

	std::string temp;
	getline(ss, temp, '\n');

	sendLine(temp);
		
}

void Client::sendBaseCapture(int baseID, int id)
{
	std::stringstream ss;
	ss << MSG_BASECAPTURE << " ";
	ss << id << " ";
	ss << baseID << " ";

	std::string temp;
	getline(ss, temp, '\n');
	sendLine(temp);
}


void Client::sendBase(float x, float y, int baseID, int id, hgeColor colour, float radie, float size, float units, float unitTime, float advancement)
{
	std::stringstream ss;
	ss << MSG_BASE << " ";
	ss << id << " ";
	ss << baseID << " ";
	ss << x << " ";
	ss << y << " ";	
	ss << colour.GetHWColor()<< " ";
	ss << radie << " ";
	ss << size << " ";
	ss << units << " ";
	ss << unitTime << " ";
	ss << advancement << " ";

	std::string temp;
	
	getline(ss, temp, '\n');
	
	sendLine(temp);	
}

void Client::sendMove( int groupID,int teamID, hgeColor colour, float toStop, float nrUnits, int origin, int dest )
{
	std::stringstream ss;
	ss << MSG_MOVE << " ";
	ss << teamID << " ";
	ss << groupID << " ";	
	ss << colour.GetHWColor()<< " ";
	ss << toStop << " ";
	ss << nrUnits << " ";
	ss << origin << " ";
	ss << dest << " ";

	std::string temp;
	getline(ss, temp, '\n');
	sendLine(temp);
}

void Client::sendNewBase(float x, float y, int baseID, int teamID, hgeColor colour, bool forceBuilt, int origin )
{
	std::stringstream ss;
	ss << MSG_NEWBASE << " ";
	ss << teamID << " ";
	ss << baseID << " ";
	ss << x << " ";
	ss << y << " ";	
	ss << colour.GetHWColor()<< " ";
	ss << forceBuilt << " ";
	ss << origin << " ";

	std::string temp;
	getline(ss, temp, '\n');
	sendLine(temp);
}

void Client::sendRmvGroup(int groupID, int id)
{
	std::stringstream ss;
	ss << MSG_RMVGROUP << " ";
	ss << id << " ";
	ss << groupID << " ";

	std::string temp;
	getline(ss, temp, '\n');
	sendLine(temp);
}

void Client::sendStart()
{
	std::stringstream ss;
	ss << MSG_START << " ";
	std::string temp;
	getline(ss, temp, '\n');
	sendLine(temp);
}

void Client::sendArrive(Group *g)
{
	std::string temp = g->msgArrive();
	sendLine(temp);	
}

void Client::sendArrive(int teamId, int groupId) // WRONG!!!
{
	std::stringstream ss;
	ss << MSG_ARRIVE << " ";
	ss << teamId << " ";
	ss << groupId << " ";

	std::string temp;
	ss >> temp;
	sendLine(temp);	
}

void Client::close()
{
	closesocket(Socket);
	end();
}


void Client::end()
{
	WSACleanup();
}
