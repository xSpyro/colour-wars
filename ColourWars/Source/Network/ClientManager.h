#ifndef CLIENTMANAGER_H
#define CLIENTMANAGER_H
#include "Client.h"
#include <sstream>

class Game;
class ClientManager : public Client
{
public:
	ClientManager();
	virtual ~ClientManager();
	void setListener( Game * pg )
	{
		this->pg = pg;
	}
	void onReadLine(const std::string & line);
	bool mPlayerConnected;
private:
	Game *pg;
	
};
#endif