#ifndef SERVER_H
#define SERVER_H

#include <winsock2.h>
#include <hgecolor.h>
#include <iostream>
#include <vector>


class Game;
class Server
{
public:
	Server();

	void setGame(Game * game);

	int run(int port);
	int update(float dt);
	bool acceptClients();
	bool allConnected();
	void end();

	int getNewBase();
	int getNewGroup();

	void removeBase(int id);
	void removeGroup(int id);

	void onReadLine(std::string line, int from);
	void sendLine(const std::string &line);

	bool newBase(hgeColor col, float x, float y, int units, int id, int teamId, bool forceBuilt, int origin);
	bool move(int teamID, hgeColor colour, float toStop, float nrUnits, int origin, int dest);
	bool rmvGroup(int groupID, int id);
	bool sacrifice(int baseID, int teamID, float units);


private:

	static const int mMaxClients = 1;	//�ndra denna variabel f�r att kunna ha fler clienter
	int mNrOfClients;
	SOCKET Socket[mMaxClients];
	SOCKET ServerSocket;

	SOCKADDR_IN SockAddr;
	sockaddr sockAddrClient;

	char szHistory[10000];
	
	int mBase;
	int mGroup;
	
	Game * mGame;

	float mTime;
	bool mSendBase;
	bool mStart;

	WSADATA WsaDat;
	//SOCKET Socket;
	//SOCKADDR_IN serverInf;
	int mPort;
	int mResult;

	std::vector<int> mEmptyGroup;
	std::vector<int> mEmptyBase;
	int mLimGroup;
	int mLimBase;

	std::vector<std::string> mLine;
};
#endif