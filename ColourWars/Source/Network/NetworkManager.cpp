#include "NetworkManager.h"

NetworkManager::NetworkManager()
{
	//mClientManager.setListener(this);
	
	mClient = new ClientManager();
}

NetworkManager::~NetworkManager()
{
}

void NetworkManager::connect(int port, const char *host)
{
	mClient->connectTo(port, host);

}

void NetworkManager::sendMsg(const std::string &msg)
{
	mClient->sendLine(msg);
}

void NetworkManager::update()
{
}

void NetworkManager::errorMessage(const std::string &msg)
{
	std::cout<<msg<<std::endl;
}

void NetworkManager::parse(const std::string &msg)
{
	if ( msg.find("ERROR") != std::string::npos )
		errorMessage(msg);
}



/*
________________________________________________________________________________
Example of use of da server =)

________________________________________________________________________________
#include "Server.h"
#include "Client.h"
#include "NetworkManager.h"
#include <iostream>


int main()
{
	int choice;
	Server s;
	Client * c = new ClientManager();
	NetworkManager t;
	


	
	int port = 6667;
	//just some random testing of da struct sending =)
	int groupID = 33;
	int x = 12;

	std::cout<<"Server? 1, 0"<<std::endl;
	std::cin>>choice;
	if ( choice == 1 ) 
		s.run(port);
	else
	{
		std::stringstream ss;
		ss << MSG_BASE << " ";
		ss << groupID << " ";
		
		ss << x << " ";

		std::string temp;
		ss>>temp;
		//std::string t = "B�G";

		
		c->connectTo(port, "localhost");
		c->sendLine(temp);
		//c->sendLine(t);
		
		
		//recieve �r den som ska anv�ndas, ska k�ras hela tiden. 
		bool FUOLEG;
		while ( true ) 
		{
			FUOLEG = c->recieve();
			if ( FUOLEG == false ) 
				break;
		}		
	}

	return 0;
}


*/