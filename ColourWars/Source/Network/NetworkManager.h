#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H
#include "Client.h"
#include "ClientManager.h"

class NetworkManager
{
public:
	NetworkManager();
	virtual ~NetworkManager();
	void sendMsg(const std::string &msg);
	void errorMessage(const std::string &msg);
	void parse(const std::string & msg);
	void connect(int port, const char *host);
	void update();

private:
	int mPort;
	std::string mHost;
	Client * mClient;
	ClientManager mClientManager;
};
#endif